import 'package:firebase_auth/firebase_auth.dart';
import 'package:fireflutter/app/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:get/get.dart';

class AuthController extends GetxController {
  FirebaseAuth auth = FirebaseAuth.instance;

  GoogleSignIn gSign = GoogleSignIn();

  Stream<User?> streamAuthStatus() {
    return auth.authStateChanges();
  }

  void signup(String email, String password, String password2,
      BuildContext context) async {
    if (password2 != password) {
      var snackBar = SnackBar(
          content: Text("Password must match"), backgroundColor: Colors.red);
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
      return;
    }

    try {
      UserCredential userCredential = await auth.createUserWithEmailAndPassword(
          email: email, password: password);
      login(email, password, context);
    } on FirebaseAuthException catch (e) {
      var err = "";
      if (e.code == 'weak-password') {
        err = 'The password provided is too weak.';
      } else if (e.code == 'email-already-in-use') {
        err = ('The account already exists for that email.');
      } else {
        err = 'Fill in the forms';
      }
      var snackBar = SnackBar(content: Text(err), backgroundColor: Colors.red);
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    } catch (e) {
      print(e);
    }
  }

  void login(String email, String password, BuildContext context) async {
    try {
      UserCredential userCredential = await auth.signInWithEmailAndPassword(
          email: email, password: password);
      // print(userCredential);
      var snackBar = SnackBar(
          content: Text("Success! Logging in"), backgroundColor: Colors.green);
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
      Get.offAllNamed(Routes.HOME);
    } on FirebaseAuthException catch (e) {
      var err = "";

      if (e.code == 'user-not-found') {
        print('No user found for that email.');
        err = 'No user found for that email.';
      } else if (e.code == 'wrong-password') {
        print('Wrong password provided for that user.');
        err = 'Wrong password provided for that user.';
      } else {
        err = 'Cannot login. Check if your email and password are correct';
      }
      var snackBar = SnackBar(content: Text(err), backgroundColor: Colors.red);
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  void logout() async {
    Get.offAllNamed(Routes.GETSTARTED);
    try {
      await gSign.disconnect();
    } catch (e) {
      print(e);
    }
    await auth.signOut();
  }

  void signInWithGoogle(BuildContext context) async {
    try {
      // Trigger the authentication flow
      final GoogleSignInAccount? googleUser = await gSign.signIn();

      // Obtain the auth details from the request
      final GoogleSignInAuthentication? googleAuth =
          await googleUser?.authentication;

      // Create a new credential
      final credential = GoogleAuthProvider.credential(
        accessToken: googleAuth?.accessToken,
        idToken: googleAuth?.idToken,
      );

      var snackBar = SnackBar(
          content: Text("Success! Logging in"), backgroundColor: Colors.green);
      ScaffoldMessenger.of(context).showSnackBar(snackBar);

      // Once signed in, return the UserCredential
      await auth.signInWithCredential(credential);

      Get.offAllNamed(Routes.HOME);
    } catch (e) {
      var snackBar = SnackBar(
          content: Text("Select a valid google account"),
          backgroundColor: Colors.red);
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }
}
