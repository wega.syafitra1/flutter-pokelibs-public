import 'package:fireflutter/app/modules/poke/models/poke_model.dart';
import 'package:fireflutter/app/modules/poke/models/pokemon_data.dart';
import 'package:http/http.dart' as http;

class RemoteServices {
  static var client = http.Client();

  static Future<void> fetchPoke() async {
    var output = <Pokemon>[];
    var response = await http.get(
      Uri.parse("https://pokeapi.co/api/v2/pokemon?limit=50&offset=1"),
    );

    if (response.statusCode == 200) {
      var body = response.body;
      var objets = pokeURLFromJson(body);
      // print(objets.results[0].name);

      for (Results res in objets.results) {
        var response_poke = await http.get(Uri.parse("${res.url}"));

        if (response_poke.statusCode == 200) {
          var body2 = response_poke.body;
          var poke = pokemonFromJson(body2);
          output.add(poke);
        }

        // print(res.name + ", " + res.url);
        // print(poke.name! + ", " + poke.stats![0].baseStat.toString());
        // print(response_poke.body);
      }
    } else {
      // print("something happened");
    }

    // return output;
  }
}
/*
void main(List<String> args) async {
  var client = http.Client();
  var response = await http.get(
    Uri.parse("https://pokeapi.co/api/v2/pokemon?limit=50&offset=1"),
  );

  if (response.statusCode == 200) {
    var body = response.body;
    var objets = pokeURLFromJson(body);
    print(objets.results[0].name);

    for (Results res in objets.results) {
      var response_poke = await http.get(Uri.parse("${res.url}"));

      var body2 = response_poke.body;
      var poke = pokemonFromJson(body2);

      print(res.name + ", " + res.url);
      print(poke.name! + ", " + poke.stats![0].baseStat.toString());
      // print(response_poke.body);
    }
  } else {
    print("something happened");
  }
}
*/