import 'package:fireflutter/app/modules/poke/controllers/poke_controller.dart';
import 'package:fireflutter/app/modules/poke/models/pokemon_data.dart';
import 'package:fireflutter/app/modules/poke/views/poke_view.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/poke_detail_controller.dart';

class PokeDetailView extends GetView<PokeDetailController> {
  PokeController con = Get.put(PokeController());
  // Pokemon current = Get.find<PokeController>().currentPoke.last;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // backgroundColor: Colors.transparent,
        appBar: AppBar(
          title: Text("Pokemon Detail", style: TextStyle(fontSize: 20)),
          centerTitle: true,
          toolbarHeight: 50,
          backgroundColor: Colors.transparent,
        ),
        body: SingleChildScrollView(
          child: Obx(() => Container(
                decoration: BoxDecoration(
                    gradient: buildGradient(
                        buildColor(
                            con.currentPoke.last.types![0].type!.name ?? "0"),
                        con.currentPoke.last.types!.length > 1
                            ? buildColor(
                                con.currentPoke.last.types![1].type!.name ??
                                    "0")
                            : buildColor(
                                con.currentPoke.last.types![0].type!.name ??
                                    "0"))),
                child: Align(
                  alignment: Alignment.center,
                  child: Column(
                    children: [
                      SizedBox(height: 40),
                      Card(
                        child: Column(
                          children: [
                            Image.network(
                              con.currentPoke.last.sprites!.other!
                                  .official_artwork!.frontDefault
                                  .toString(),
                              height: 200,
                            ),
                            Container(
                              width: 340,
                              child: Padding(
                                padding: const EdgeInsets.all(20.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      "${con.currentPoke.last.name ?? 'No name'} #${con.currentPoke.last.id ?? 'No name'}"
                                          .capitalize!,
                                      style: TextStyle(
                                          fontSize: 30,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    SizedBox(
                                      height: 40,
                                    ),
                                    Text(
                                        "Height : ${con.currentPoke.last.height!.toDouble() / 10} m",
                                        style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold)),
                                    SizedBox(height: 10),
                                    Text(
                                        "Weight : ${con.currentPoke.last.weight!.toDouble() / 10} kg",
                                        style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold)),
                                    SizedBox(height: 30),
                                    Text("Abilities",
                                        style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold)),
                                    Row(
                                      children: [
                                        con.currentPoke.last.abilities!.length >
                                                1
                                            ? Column(
                                                children: [
                                                  Row(
                                                    children: [
                                                      buildChip(
                                                          Colors.grey,
                                                          con
                                                                  .currentPoke
                                                                  .last
                                                                  .abilities![0]
                                                                  .ability!
                                                                  .name ??
                                                              "0"),
                                                      SizedBox(
                                                        width: 10,
                                                      ),
                                                      buildChip(
                                                          Colors.grey,
                                                          con
                                                                  .currentPoke
                                                                  .last
                                                                  .abilities![1]
                                                                  .ability!
                                                                  .name ??
                                                              "0")
                                                    ],
                                                  )
                                                ],
                                              )
                                            : Column(
                                                children: [
                                                  buildChip(
                                                      Colors.grey,
                                                      con
                                                              .currentPoke
                                                              .last
                                                              .abilities![0]
                                                              .ability!
                                                              .name ??
                                                          "0"),
                                                ],
                                              ),
                                      ],
                                    ),
                                    SizedBox(height: 30),
                                    Text("Types",
                                        style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold)),
                                    Row(
                                      children: [
                                        con.currentPoke.last.types!.length > 1
                                            ? Column(
                                                children: [
                                                  Row(
                                                    children: [
                                                      buildChip(
                                                          buildColor(con
                                                                  .currentPoke
                                                                  .last
                                                                  .types![0]
                                                                  .type!
                                                                  .name ??
                                                              "0"),
                                                          con
                                                                  .currentPoke
                                                                  .last
                                                                  .types![0]
                                                                  .type!
                                                                  .name ??
                                                              "0"),
                                                      SizedBox(
                                                        width: 10,
                                                      ),
                                                      buildChip(
                                                          buildColor(con
                                                                  .currentPoke
                                                                  .last
                                                                  .types![1]
                                                                  .type!
                                                                  .name ??
                                                              "0"),
                                                          con
                                                                  .currentPoke
                                                                  .last
                                                                  .types![1]
                                                                  .type!
                                                                  .name ??
                                                              "0")
                                                    ],
                                                  )
                                                ],
                                              )
                                            : Column(
                                                children: [
                                                  buildChip(
                                                      buildColor(con
                                                              .currentPoke
                                                              .last
                                                              .types![0]
                                                              .type!
                                                              .name ??
                                                          "0"),
                                                      con
                                                              .currentPoke
                                                              .last
                                                              .types![0]
                                                              .type!
                                                              .name ??
                                                          "0"),
                                                ],
                                              ),
                                      ],
                                    ),
                                    SizedBox(height: 30),
                                    Text("Stats",
                                        style: TextStyle(
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold)),
                                    SizedBox(height: 20),
                                    Text("Base Experience",
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.bold)),
                                    SizedBox(height: 10),
                                    buildStat("Base Experience", Colors.pink,
                                        con.currentPoke.last.baseExperience!),
                                    SizedBox(height: 10),
                                    Text("HP",
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.bold)),
                                    SizedBox(height: 10),
                                    buildStat(
                                        "HP",
                                        Colors.red,
                                        con.currentPoke.last.stats![0]
                                                .baseStat ??
                                            0),
                                    SizedBox(height: 10),
                                    Text("Attack",
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.bold)),
                                    SizedBox(height: 10),
                                    buildStat(
                                        "Attack",
                                        Colors.yellow,
                                        con.currentPoke.last.stats![1]
                                                .baseStat ??
                                            0),
                                    SizedBox(height: 10),
                                    Text("Defense",
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.bold)),
                                    SizedBox(height: 10),
                                    buildStat(
                                        "Defense",
                                        Colors.blue,
                                        con.currentPoke.last.stats![2]
                                                .baseStat ??
                                            0),
                                    SizedBox(height: 10),
                                    Text("Special Attack",
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.bold)),
                                    SizedBox(height: 10),
                                    buildStat(
                                        "Special Attack",
                                        Colors.teal,
                                        con.currentPoke.last.stats![3]
                                                .baseStat ??
                                            0),
                                    SizedBox(height: 10),
                                    Text("Special Defense",
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.bold)),
                                    SizedBox(height: 10),
                                    buildStat(
                                        "Special Defensse",
                                        Colors.tealAccent,
                                        con.currentPoke.last.stats![4]
                                                .baseStat ??
                                            0),
                                    SizedBox(height: 10),
                                    Text("Speed",
                                        style: TextStyle(
                                            fontSize: 17,
                                            fontWeight: FontWeight.bold)),
                                    SizedBox(height: 10),
                                    buildStat(
                                        "Speed",
                                        Colors.blue.shade100,
                                        con.currentPoke.last.stats![5]
                                                .baseStat ??
                                            0),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              )),
        ));
  }
}

LinearPercentIndicator buildStat(String name, Color color, num value) {
  return LinearPercentIndicator(
    animation: true,
    animationDuration: 3000,
    lineHeight: 20.0,
    // trailing: Text("${value}"),
    percent: name == "Base Experience" ? value / (500) : value / (150),
    center: Text("${value}",
        style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold)),
    linearStrokeCap: LinearStrokeCap.butt,
    progressColor: color,
  );
}
