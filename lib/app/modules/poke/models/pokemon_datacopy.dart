import 'dart:convert';

Pokemon pokemonFromJson(String str) => Pokemon.fromJson(json.decode(str));

String pokemonToJson(Pokemon data) => json.encode(data.toJson());

class Pokemon {
  Pokemon({
    required this.abilities,
    required this.baseExperience,
    required this.forms,
    required this.gameIndices,
    required this.height,
    required this.heldItems,
    required this.id,
    required this.isDefault,
    required this.locationAreaEncounters,
    required this.moves,
    required this.name,
    required this.order,
    required this.pastTypes,
    required this.species,
    required this.sprites,
    required this.stats,
    required this.types,
    required this.weight,
  });
  late final List<Abilities> abilities;
  late final int baseExperience;
  late final List<Forms> forms;
  late final List<GameIndices> gameIndices;
  late final int height;
  late final List<HeldItems> heldItems;
  late final int id;
  late final bool isDefault;
  late final String locationAreaEncounters;
  late final List<Moves> moves;
  late final String name;
  late final int order;
  late final List<dynamic> pastTypes;
  late final Species species;
  late final Sprites sprites;
  late final List<Stats> stats;
  late final List<Types> types;
  late final int weight;

  Pokemon.fromJson(Map<String, dynamic> json) {
    abilities =
        List.from(json['abilities']).map((e) => Abilities.fromJson(e)).toList();
    baseExperience = json['base_experience'];
    forms = List.from(json['forms']).map((e) => Forms.fromJson(e)).toList();
    gameIndices = List.from(json['game_indices'])
        .map((e) => GameIndices.fromJson(e))
        .toList();
    height = json['height'];
    heldItems = List.from(json['held_items'])
        .map((e) => HeldItems.fromJson(e))
        .toList();
    id = json['id'];
    isDefault = json['is_default'];
    locationAreaEncounters = json['location_area_encounters'];
    moves = List.from(json['moves']).map((e) => Moves.fromJson(e)).toList();
    name = json['name'];
    order = json['order'];
    pastTypes = List.castFrom<dynamic, dynamic>(json['past_types']);
    species = Species.fromJson(json['species']);
    sprites = Sprites.fromJson(json['sprites']);
    stats = List.from(json['stats']).map((e) => Stats.fromJson(e)).toList();
    types = List.from(json['types']).map((e) => Types.fromJson(e)).toList();
    weight = json['weight'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['abilities'] = abilities.map((e) => e.toJson()).toList();
    _data['base_experience'] = baseExperience;
    _data['forms'] = forms.map((e) => e.toJson()).toList();
    _data['game_indices'] = gameIndices.map((e) => e.toJson()).toList();
    _data['height'] = height;
    _data['held_items'] = heldItems.map((e) => e.toJson()).toList();
    _data['id'] = id;
    _data['is_default'] = isDefault;
    _data['location_area_encounters'] = locationAreaEncounters;
    _data['moves'] = moves.map((e) => e.toJson()).toList();
    _data['name'] = name;
    _data['order'] = order;
    _data['past_types'] = pastTypes;
    _data['species'] = species.toJson();
    _data['sprites'] = sprites.toJson();
    _data['stats'] = stats.map((e) => e.toJson()).toList();
    _data['types'] = types.map((e) => e.toJson()).toList();
    _data['weight'] = weight;
    return _data;
  }
}

class Abilities {
  Abilities({
    required this.ability,
    required this.isHidden,
    required this.slot,
  });
  late final Ability ability;
  late final bool isHidden;
  late final int slot;

  Abilities.fromJson(Map<String, dynamic> json) {
    ability = Ability.fromJson(json['ability']);
    isHidden = json['is_hidden'];
    slot = json['slot'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['ability'] = ability.toJson();
    _data['is_hidden'] = isHidden;
    _data['slot'] = slot;
    return _data;
  }
}

class Ability {
  Ability({
    required this.name,
    required this.url,
  });
  late final String name;
  late final String url;

  Ability.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['name'] = name;
    _data['url'] = url;
    return _data;
  }
}

class Forms {
  Forms({
    required this.name,
    required this.url,
  });
  late final String name;
  late final String url;

  Forms.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['name'] = name;
    _data['url'] = url;
    return _data;
  }
}

class GameIndices {
  GameIndices({
    required this.gameIndex,
    required this.version,
  });
  late final int gameIndex;
  late final Version version;

  GameIndices.fromJson(Map<String, dynamic> json) {
    gameIndex = json['game_index'];
    version = Version.fromJson(json['version']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['game_index'] = gameIndex;
    _data['version'] = version.toJson();
    return _data;
  }
}

class Version {
  Version({
    required this.name,
    required this.url,
  });
  late final String name;
  late final String url;

  Version.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['name'] = name;
    _data['url'] = url;
    return _data;
  }
}

class HeldItems {
  HeldItems({
    required this.item,
    required this.versionDetails,
  });
  late final Item item;
  late final List<VersionDetails> versionDetails;

  HeldItems.fromJson(Map<String, dynamic> json) {
    item = Item.fromJson(json['item']);
    versionDetails = List.from(json['version_details'])
        .map((e) => VersionDetails.fromJson(e))
        .toList();
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['item'] = item.toJson();
    _data['version_details'] = versionDetails.map((e) => e.toJson()).toList();
    return _data;
  }
}

class Item {
  Item({
    required this.name,
    required this.url,
  });
  late final String name;
  late final String url;

  Item.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['name'] = name;
    _data['url'] = url;
    return _data;
  }
}

class VersionDetails {
  VersionDetails({
    required this.rarity,
    required this.version,
  });
  late final int rarity;
  late final Version version;

  VersionDetails.fromJson(Map<String, dynamic> json) {
    rarity = json['rarity'];
    version = Version.fromJson(json['version']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['rarity'] = rarity;
    _data['version'] = version.toJson();
    return _data;
  }
}

class Moves {
  Moves({
    required this.move,
    required this.versionGroupDetails,
  });
  late final Move move;
  late final List<VersionGroupDetails> versionGroupDetails;

  Moves.fromJson(Map<String, dynamic> json) {
    move = Move.fromJson(json['move']);
    versionGroupDetails = List.from(json['version_group_details'])
        .map((e) => VersionGroupDetails.fromJson(e))
        .toList();
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['move'] = move.toJson();
    _data['version_group_details'] =
        versionGroupDetails.map((e) => e.toJson()).toList();
    return _data;
  }
}

class Move {
  Move({
    required this.name,
    required this.url,
  });
  late final String name;
  late final String url;

  Move.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['name'] = name;
    _data['url'] = url;
    return _data;
  }
}

class VersionGroupDetails {
  VersionGroupDetails({
    required this.levelLearnedAt,
    required this.moveLearnMethod,
    required this.versionGroup,
  });
  late final int levelLearnedAt;
  late final MoveLearnMethod moveLearnMethod;
  late final VersionGroup versionGroup;

  VersionGroupDetails.fromJson(Map<String, dynamic> json) {
    levelLearnedAt = json['level_learned_at'];
    moveLearnMethod = MoveLearnMethod.fromJson(json['move_learn_method']);
    versionGroup = VersionGroup.fromJson(json['version_group']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['level_learned_at'] = levelLearnedAt;
    _data['move_learn_method'] = moveLearnMethod.toJson();
    _data['version_group'] = versionGroup.toJson();
    return _data;
  }
}

class MoveLearnMethod {
  MoveLearnMethod({
    required this.name,
    required this.url,
  });
  late final String name;
  late final String url;

  MoveLearnMethod.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['name'] = name;
    _data['url'] = url;
    return _data;
  }
}

class VersionGroup {
  VersionGroup({
    required this.name,
    required this.url,
  });
  late final String name;
  late final String url;

  VersionGroup.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['name'] = name;
    _data['url'] = url;
    return _data;
  }
}

class Species {
  Species({
    required this.name,
    required this.url,
  });
  late final String name;
  late final String url;

  Species.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['name'] = name;
    _data['url'] = url;
    return _data;
  }
}

class Sprites {
  Sprites({
    required this.backDefault,
    required this.backFemale,
    required this.backShiny,
    required this.backShinyFemale,
    required this.frontDefault,
    required this.frontFemale,
    required this.frontShiny,
    required this.frontShinyFemale,
    required this.other,
    required this.versions,
  });
  late final String backDefault;
  late final String backFemale;
  late final String backShiny;
  late final String backShinyFemale;
  late final String frontDefault;
  late final String frontFemale;
  late final String frontShiny;
  late final String frontShinyFemale;
  late final Other other;
  late final Versions versions;

  Sprites.fromJson(Map<String, dynamic> json) {
    backDefault = json['back_default'];
    backFemale = json['back_female'];
    backShiny = json['back_shiny'];
    backShinyFemale = json['back_shiny_female'];
    frontDefault = json['front_default'];
    frontFemale = json['front_female'];
    frontShiny = json['front_shiny'];
    frontShinyFemale = json['front_shiny_female'];
    other = Other.fromJson(json['other']);
    versions = Versions.fromJson(json['versions']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['back_default'] = backDefault;
    _data['back_female'] = backFemale;
    _data['back_shiny'] = backShiny;
    _data['back_shiny_female'] = backShinyFemale;
    _data['front_default'] = frontDefault;
    _data['front_female'] = frontFemale;
    _data['front_shiny'] = frontShiny;
    _data['front_shiny_female'] = frontShinyFemale;
    _data['other'] = other.toJson();
    _data['versions'] = versions.toJson();
    return _data;
  }
}

class Other {
  Other({
    required this.dreamWorld,
    required this.home,
    required this.official_artwork,
  });
  late final DreamWorld dreamWorld;
  late final Home home;
  late final Official_artwork official_artwork;

  Other.fromJson(Map<String, dynamic> json) {
    dreamWorld = DreamWorld.fromJson(json['dream_world']);
    home = Home.fromJson(json['home']);
    official_artwork = Official_artwork.fromJson(json['official_artwork']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['dream_world'] = dreamWorld.toJson();
    _data['home'] = home.toJson();
    _data['official_artwork'] = official_artwork.toJson();
    return _data;
  }
}

class DreamWorld {
  DreamWorld({
    required this.frontDefault,
    this.frontFemale,
  });
  late final String frontDefault;
  late final Null frontFemale;

  DreamWorld.fromJson(Map<String, dynamic> json) {
    frontDefault = json['front_default'];
    frontFemale = null;
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['front_default'] = frontDefault;
    _data['front_female'] = frontFemale;
    return _data;
  }
}

class Home {
  Home({
    required this.frontDefault,
    required this.frontFemale,
    required this.frontShiny,
    required this.frontShinyFemale,
  });
  late final String frontDefault;
  late final String frontFemale;
  late final String frontShiny;
  late final String frontShinyFemale;

  Home.fromJson(Map<String, dynamic> json) {
    frontDefault = json['front_default'];
    frontFemale = json['front_female'];
    frontShiny = json['front_shiny'];
    frontShinyFemale = json['front_shiny_female'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['front_default'] = frontDefault;
    _data['front_female'] = frontFemale;
    _data['front_shiny'] = frontShiny;
    _data['front_shiny_female'] = frontShinyFemale;
    return _data;
  }
}

class Official_artwork {
  Official_artwork({
    required this.frontDefault,
  });
  late final String frontDefault;

  Official_artwork.fromJson(Map<String, dynamic> json) {
    frontDefault = json['front_default'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['front_default'] = frontDefault;
    return _data;
  }
}

class Versions {
  Versions({
    required this.generation_i,
    required this.generation_ii,
    required this.generation_iii,
    required this.generation_iv,
    required this.generation_v,
    required this.generation_vi,
    required this.generation_vii,
    required this.generation_viii,
  });
  late final Generation_i generation_i;
  late final Generation_ii generation_ii;
  late final Generation_iii generation_iii;
  late final Generation_iv generation_iv;
  late final Generation_v generation_v;
  late final Generation_vi generation_vi;
  late final Generation_vii generation_vii;
  late final Generation_viii generation_viii;

  Versions.fromJson(Map<String, dynamic> json) {
    generation_i = Generation_i.fromJson(json['generation_i']);
    generation_ii = Generation_ii.fromJson(json['generation_ii']);
    generation_iii = Generation_iii.fromJson(json['generation_iii']);
    generation_iv = Generation_iv.fromJson(json['generation_iv']);
    generation_v = Generation_v.fromJson(json['generation_v']);
    generation_vi = Generation_vi.fromJson(json['generation_vi']);
    generation_vii = Generation_vii.fromJson(json['generation_vii']);
    generation_viii = Generation_viii.fromJson(json['generation_viii']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['generation_i'] = generation_i.toJson();
    _data['generation_ii'] = generation_ii.toJson();
    _data['generation_iii'] = generation_iii.toJson();
    _data['generation_iv'] = generation_iv.toJson();
    _data['generation_v'] = generation_v.toJson();
    _data['generation_vi'] = generation_vi.toJson();
    _data['generation_vii'] = generation_vii.toJson();
    _data['generation_viii'] = generation_viii.toJson();
    return _data;
  }
}

class Generation_i {
  Generation_i({
    required this.red_blue,
    required this.yellow,
  });
  late final Red_blue red_blue;
  late final Yellow yellow;

  Generation_i.fromJson(Map<String, dynamic> json) {
    red_blue = Red_blue.fromJson(json['red_blue']);
    yellow = Yellow.fromJson(json['yellow']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['red_blue'] = red_blue.toJson();
    _data['yellow'] = yellow.toJson();
    return _data;
  }
}

class Red_blue {
  Red_blue({
    required this.backDefault,
    required this.backGray,
    required this.backTransparent,
    required this.frontDefault,
    required this.frontGray,
    required this.frontTransparent,
  });
  late final String backDefault;
  late final String backGray;
  late final String backTransparent;
  late final String frontDefault;
  late final String frontGray;
  late final String frontTransparent;

  Red_blue.fromJson(Map<String, dynamic> json) {
    backDefault = json['back_default'];
    backGray = json['back_gray'];
    backTransparent = json['back_transparent'];
    frontDefault = json['front_default'];
    frontGray = json['front_gray'];
    frontTransparent = json['front_transparent'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['back_default'] = backDefault;
    _data['back_gray'] = backGray;
    _data['back_transparent'] = backTransparent;
    _data['front_default'] = frontDefault;
    _data['front_gray'] = frontGray;
    _data['front_transparent'] = frontTransparent;
    return _data;
  }
}

class Yellow {
  Yellow({
    required this.backDefault,
    required this.backGray,
    required this.backTransparent,
    required this.frontDefault,
    required this.frontGray,
    required this.frontTransparent,
  });
  late final String backDefault;
  late final String backGray;
  late final String backTransparent;
  late final String frontDefault;
  late final String frontGray;
  late final String frontTransparent;

  Yellow.fromJson(Map<String, dynamic> json) {
    backDefault = json['back_default'];
    backGray = json['back_gray'];
    backTransparent = json['back_transparent'];
    frontDefault = json['front_default'];
    frontGray = json['front_gray'];
    frontTransparent = json['front_transparent'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['back_default'] = backDefault;
    _data['back_gray'] = backGray;
    _data['back_transparent'] = backTransparent;
    _data['front_default'] = frontDefault;
    _data['front_gray'] = frontGray;
    _data['front_transparent'] = frontTransparent;
    return _data;
  }
}

class Generation_ii {
  Generation_ii({
    required this.crystal,
    required this.gold,
    required this.silver,
  });
  late final Crystal crystal;
  late final Gold gold;
  late final Silver silver;

  Generation_ii.fromJson(Map<String, dynamic> json) {
    crystal = Crystal.fromJson(json['crystal']);
    gold = Gold.fromJson(json['gold']);
    silver = Silver.fromJson(json['silver']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['crystal'] = crystal.toJson();
    _data['gold'] = gold.toJson();
    _data['silver'] = silver.toJson();
    return _data;
  }
}

class Crystal {
  Crystal({
    required this.backDefault,
    required this.backShiny,
    required this.backShinyTransparent,
    required this.backTransparent,
    required this.frontDefault,
    required this.frontShiny,
    required this.frontShinyTransparent,
    required this.frontTransparent,
  });
  late final String backDefault;
  late final String backShiny;
  late final String backShinyTransparent;
  late final String backTransparent;
  late final String frontDefault;
  late final String frontShiny;
  late final String frontShinyTransparent;
  late final String frontTransparent;

  Crystal.fromJson(Map<String, dynamic> json) {
    backDefault = json['back_default'];
    backShiny = json['back_shiny'];
    backShinyTransparent = json['back_shiny_transparent'];
    backTransparent = json['back_transparent'];
    frontDefault = json['front_default'];
    frontShiny = json['front_shiny'];
    frontShinyTransparent = json['front_shiny_transparent'];
    frontTransparent = json['front_transparent'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['back_default'] = backDefault;
    _data['back_shiny'] = backShiny;
    _data['back_shiny_transparent'] = backShinyTransparent;
    _data['back_transparent'] = backTransparent;
    _data['front_default'] = frontDefault;
    _data['front_shiny'] = frontShiny;
    _data['front_shiny_transparent'] = frontShinyTransparent;
    _data['front_transparent'] = frontTransparent;
    return _data;
  }
}

class Gold {
  Gold({
    required this.backDefault,
    required this.backShiny,
    required this.frontDefault,
    required this.frontShiny,
    required this.frontTransparent,
  });
  late final String backDefault;
  late final String backShiny;
  late final String frontDefault;
  late final String frontShiny;
  late final String frontTransparent;

  Gold.fromJson(Map<String, dynamic> json) {
    backDefault = json['back_default'];
    backShiny = json['back_shiny'];
    frontDefault = json['front_default'];
    frontShiny = json['front_shiny'];
    frontTransparent = json['front_transparent'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['back_default'] = backDefault;
    _data['back_shiny'] = backShiny;
    _data['front_default'] = frontDefault;
    _data['front_shiny'] = frontShiny;
    _data['front_transparent'] = frontTransparent;
    return _data;
  }
}

class Silver {
  Silver({
    required this.backDefault,
    required this.backShiny,
    required this.frontDefault,
    required this.frontShiny,
    required this.frontTransparent,
  });
  late final String backDefault;
  late final String backShiny;
  late final String frontDefault;
  late final String frontShiny;
  late final String frontTransparent;

  Silver.fromJson(Map<String, dynamic> json) {
    backDefault = json['back_default'];
    backShiny = json['back_shiny'];
    frontDefault = json['front_default'];
    frontShiny = json['front_shiny'];
    frontTransparent = json['front_transparent'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['back_default'] = backDefault;
    _data['back_shiny'] = backShiny;
    _data['front_default'] = frontDefault;
    _data['front_shiny'] = frontShiny;
    _data['front_transparent'] = frontTransparent;
    return _data;
  }
}

class Generation_iii {
  Generation_iii({
    required this.emerald,
    required this.firered_leafgreen,
    required this.ruby_sapphire,
  });
  late final Emerald emerald;
  late final Firered_leafgreen firered_leafgreen;
  late final Ruby_sapphire ruby_sapphire;

  Generation_iii.fromJson(Map<String, dynamic> json) {
    emerald = Emerald.fromJson(json['emerald']);
    firered_leafgreen = Firered_leafgreen.fromJson(json['firered_leafgreen']);
    ruby_sapphire = Ruby_sapphire.fromJson(json['ruby_sapphire']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['emerald'] = emerald.toJson();
    _data['firered_leafgreen'] = firered_leafgreen.toJson();
    _data['ruby_sapphire'] = ruby_sapphire.toJson();
    return _data;
  }
}

class Emerald {
  Emerald({
    required this.frontDefault,
    required this.frontShiny,
  });
  late final String frontDefault;
  late final String frontShiny;

  Emerald.fromJson(Map<String, dynamic> json) {
    frontDefault = json['front_default'];
    frontShiny = json['front_shiny'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['front_default'] = frontDefault;
    _data['front_shiny'] = frontShiny;
    return _data;
  }
}

class Firered_leafgreen {
  Firered_leafgreen({
    required this.backDefault,
    required this.backShiny,
    required this.frontDefault,
    required this.frontShiny,
  });
  late final String backDefault;
  late final String backShiny;
  late final String frontDefault;
  late final String frontShiny;

  Firered_leafgreen.fromJson(Map<String, dynamic> json) {
    backDefault = json['back_default'];
    backShiny = json['back_shiny'];
    frontDefault = json['front_default'];
    frontShiny = json['front_shiny'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['back_default'] = backDefault;
    _data['back_shiny'] = backShiny;
    _data['front_default'] = frontDefault;
    _data['front_shiny'] = frontShiny;
    return _data;
  }
}

class Ruby_sapphire {
  Ruby_sapphire({
    required this.backDefault,
    required this.backShiny,
    required this.frontDefault,
    required this.frontShiny,
  });
  late final String backDefault;
  late final String backShiny;
  late final String frontDefault;
  late final String frontShiny;

  Ruby_sapphire.fromJson(Map<String, dynamic> json) {
    backDefault = json['back_default'];
    backShiny = json['back_shiny'];
    frontDefault = json['front_default'];
    frontShiny = json['front_shiny'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['back_default'] = backDefault;
    _data['back_shiny'] = backShiny;
    _data['front_default'] = frontDefault;
    _data['front_shiny'] = frontShiny;
    return _data;
  }
}

class Generation_iv {
  Generation_iv({
    required this.diamond_pearl,
    required this.heartgold_soulsilver,
    required this.platinum,
  });
  late final Diamond_pearl diamond_pearl;
  late final Heartgold_soulsilver heartgold_soulsilver;
  late final Platinum platinum;

  Generation_iv.fromJson(Map<String, dynamic> json) {
    diamond_pearl = Diamond_pearl.fromJson(json['diamond_pearl']);
    heartgold_soulsilver =
        Heartgold_soulsilver.fromJson(json['heartgold_soulsilver']);
    platinum = Platinum.fromJson(json['platinum']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['diamond_pearl'] = diamond_pearl.toJson();
    _data['heartgold_soulsilver'] = heartgold_soulsilver.toJson();
    _data['platinum'] = platinum.toJson();
    return _data;
  }
}

class Diamond_pearl {
  Diamond_pearl({
    required this.backDefault,
    required this.backFemale,
    required this.backShiny,
    required this.backShinyFemale,
    required this.frontDefault,
    required this.frontFemale,
    required this.frontShiny,
    required this.frontShinyFemale,
  });
  late final String backDefault;
  late final String backFemale;
  late final String backShiny;
  late final String backShinyFemale;
  late final String frontDefault;
  late final String frontFemale;
  late final String frontShiny;
  late final String frontShinyFemale;

  Diamond_pearl.fromJson(Map<String, dynamic> json) {
    backDefault = json['back_default'];
    backFemale = json['back_female'];
    backShiny = json['back_shiny'];
    backShinyFemale = json['back_shiny_female'];
    frontDefault = json['front_default'];
    frontFemale = json['front_female'];
    frontShiny = json['front_shiny'];
    frontShinyFemale = json['front_shiny_female'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['back_default'] = backDefault;
    _data['back_female'] = backFemale;
    _data['back_shiny'] = backShiny;
    _data['back_shiny_female'] = backShinyFemale;
    _data['front_default'] = frontDefault;
    _data['front_female'] = frontFemale;
    _data['front_shiny'] = frontShiny;
    _data['front_shiny_female'] = frontShinyFemale;
    return _data;
  }
}

class Heartgold_soulsilver {
  Heartgold_soulsilver({
    required this.backDefault,
    required this.backFemale,
    required this.backShiny,
    required this.backShinyFemale,
    required this.frontDefault,
    required this.frontFemale,
    required this.frontShiny,
    required this.frontShinyFemale,
  });
  late final String backDefault;
  late final String backFemale;
  late final String backShiny;
  late final String backShinyFemale;
  late final String frontDefault;
  late final String frontFemale;
  late final String frontShiny;
  late final String frontShinyFemale;

  Heartgold_soulsilver.fromJson(Map<String, dynamic> json) {
    backDefault = json['back_default'];
    backFemale = json['back_female'];
    backShiny = json['back_shiny'];
    backShinyFemale = json['back_shiny_female'];
    frontDefault = json['front_default'];
    frontFemale = json['front_female'];
    frontShiny = json['front_shiny'];
    frontShinyFemale = json['front_shiny_female'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['back_default'] = backDefault;
    _data['back_female'] = backFemale;
    _data['back_shiny'] = backShiny;
    _data['back_shiny_female'] = backShinyFemale;
    _data['front_default'] = frontDefault;
    _data['front_female'] = frontFemale;
    _data['front_shiny'] = frontShiny;
    _data['front_shiny_female'] = frontShinyFemale;
    return _data;
  }
}

class Platinum {
  Platinum({
    required this.backDefault,
    required this.backFemale,
    required this.backShiny,
    required this.backShinyFemale,
    required this.frontDefault,
    required this.frontFemale,
    required this.frontShiny,
    required this.frontShinyFemale,
  });
  late final String backDefault;
  late final String backFemale;
  late final String backShiny;
  late final String backShinyFemale;
  late final String frontDefault;
  late final String frontFemale;
  late final String frontShiny;
  late final String frontShinyFemale;

  Platinum.fromJson(Map<String, dynamic> json) {
    backDefault = json['back_default'];
    backFemale = json['back_female'];
    backShiny = json['back_shiny'];
    backShinyFemale = json['back_shiny_female'];
    frontDefault = json['front_default'];
    frontFemale = json['front_female'];
    frontShiny = json['front_shiny'];
    frontShinyFemale = json['front_shiny_female'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['back_default'] = backDefault;
    _data['back_female'] = backFemale;
    _data['back_shiny'] = backShiny;
    _data['back_shiny_female'] = backShinyFemale;
    _data['front_default'] = frontDefault;
    _data['front_female'] = frontFemale;
    _data['front_shiny'] = frontShiny;
    _data['front_shiny_female'] = frontShinyFemale;
    return _data;
  }
}

class Generation_v {
  Generation_v({
    required this.black_white,
  });
  late final Black_white black_white;

  Generation_v.fromJson(Map<String, dynamic> json) {
    black_white = Black_white.fromJson(json['black_white']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['black_white'] = black_white.toJson();
    return _data;
  }
}

class Black_white {
  Black_white({
    required this.animated,
    required this.backDefault,
    required this.backFemale,
    required this.backShiny,
    required this.backShinyFemale,
    required this.frontDefault,
    required this.frontFemale,
    required this.frontShiny,
    required this.frontShinyFemale,
  });
  late final Animated animated;
  late final String backDefault;
  late final String backFemale;
  late final String backShiny;
  late final String backShinyFemale;
  late final String frontDefault;
  late final String frontFemale;
  late final String frontShiny;
  late final String frontShinyFemale;

  Black_white.fromJson(Map<String, dynamic> json) {
    animated = Animated.fromJson(json['animated']);
    backDefault = json['back_default'];
    backFemale = json['back_female'];
    backShiny = json['back_shiny'];
    backShinyFemale = json['back_shiny_female'];
    frontDefault = json['front_default'];
    frontFemale = json['front_female'];
    frontShiny = json['front_shiny'];
    frontShinyFemale = json['front_shiny_female'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['animated'] = animated.toJson();
    _data['back_default'] = backDefault;
    _data['back_female'] = backFemale;
    _data['back_shiny'] = backShiny;
    _data['back_shiny_female'] = backShinyFemale;
    _data['front_default'] = frontDefault;
    _data['front_female'] = frontFemale;
    _data['front_shiny'] = frontShiny;
    _data['front_shiny_female'] = frontShinyFemale;
    return _data;
  }
}

class Animated {
  Animated({
    required this.backDefault,
    required this.backFemale,
    required this.backShiny,
    required this.backShinyFemale,
    required this.frontDefault,
    required this.frontFemale,
    required this.frontShiny,
    required this.frontShinyFemale,
  });
  late final String backDefault;
  late final String backFemale;
  late final String backShiny;
  late final String backShinyFemale;
  late final String frontDefault;
  late final String frontFemale;
  late final String frontShiny;
  late final String frontShinyFemale;

  Animated.fromJson(Map<String, dynamic> json) {
    backDefault = json['back_default'];
    backFemale = json['back_female'];
    backShiny = json['back_shiny'];
    backShinyFemale = json['back_shiny_female'];
    frontDefault = json['front_default'];
    frontFemale = json['front_female'];
    frontShiny = json['front_shiny'];
    frontShinyFemale = json['front_shiny_female'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['back_default'] = backDefault;
    _data['back_female'] = backFemale;
    _data['back_shiny'] = backShiny;
    _data['back_shiny_female'] = backShinyFemale;
    _data['front_default'] = frontDefault;
    _data['front_female'] = frontFemale;
    _data['front_shiny'] = frontShiny;
    _data['front_shiny_female'] = frontShinyFemale;
    return _data;
  }
}

class Generation_vi {
  Generation_vi({
    required this.omegaruby_alphasapphire,
    required this.x_y,
  });
  late final Omegaruby_alphasapphire omegaruby_alphasapphire;
  late final X_y x_y;

  Generation_vi.fromJson(Map<String, dynamic> json) {
    omegaruby_alphasapphire =
        Omegaruby_alphasapphire.fromJson(json['omegaruby_alphasapphire']);
    x_y = X_y.fromJson(json['x_y']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['omegaruby_alphasapphire'] = omegaruby_alphasapphire.toJson();
    _data['x_y'] = x_y.toJson();
    return _data;
  }
}

class Omegaruby_alphasapphire {
  Omegaruby_alphasapphire({
    required this.frontDefault,
    required this.frontFemale,
    required this.frontShiny,
    required this.frontShinyFemale,
  });
  late final String frontDefault;
  late final String frontFemale;
  late final String frontShiny;
  late final String frontShinyFemale;

  Omegaruby_alphasapphire.fromJson(Map<String, dynamic> json) {
    frontDefault = json['front_default'];
    frontFemale = json['front_female'];
    frontShiny = json['front_shiny'];
    frontShinyFemale = json['front_shiny_female'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['front_default'] = frontDefault;
    _data['front_female'] = frontFemale;
    _data['front_shiny'] = frontShiny;
    _data['front_shiny_female'] = frontShinyFemale;
    return _data;
  }
}

class X_y {
  X_y({
    required this.frontDefault,
    required this.frontFemale,
    required this.frontShiny,
    required this.frontShinyFemale,
  });
  late final String frontDefault;
  late final String frontFemale;
  late final String frontShiny;
  late final String frontShinyFemale;

  X_y.fromJson(Map<String, dynamic> json) {
    frontDefault = json['front_default'];
    frontFemale = json['front_female'];
    frontShiny = json['front_shiny'];
    frontShinyFemale = json['front_shiny_female'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['front_default'] = frontDefault;
    _data['front_female'] = frontFemale;
    _data['front_shiny'] = frontShiny;
    _data['front_shiny_female'] = frontShinyFemale;
    return _data;
  }
}

class Generation_vii {
  Generation_vii({
    required this.icons,
    required this.ultra_sun_ultra_moon,
  });
  late final Icons icons;
  late final Ultra_sun_ultra_moon ultra_sun_ultra_moon;

  Generation_vii.fromJson(Map<String, dynamic> json) {
    icons = Icons.fromJson(json['icons']);
    ultra_sun_ultra_moon =
        Ultra_sun_ultra_moon.fromJson(json['ultra_sun_ultra_moon']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['icons'] = icons.toJson();
    _data['ultra_sun_ultra_moon'] = ultra_sun_ultra_moon.toJson();
    return _data;
  }
}

class Icons {
  Icons({
    required this.frontDefault,
    this.frontFemale,
  });
  late final String frontDefault;
  late final Null frontFemale;

  Icons.fromJson(Map<String, dynamic> json) {
    frontDefault = json['front_default'];
    frontFemale = null;
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['front_default'] = frontDefault;
    _data['front_female'] = frontFemale;
    return _data;
  }
}

class Ultra_sun_ultra_moon {
  Ultra_sun_ultra_moon({
    required this.frontDefault,
    required this.frontFemale,
    required this.frontShiny,
    required this.frontShinyFemale,
  });
  late final String frontDefault;
  late final String frontFemale;
  late final String frontShiny;
  late final String frontShinyFemale;

  Ultra_sun_ultra_moon.fromJson(Map<String, dynamic> json) {
    frontDefault = json['front_default'];
    frontFemale = json['front_female'];
    frontShiny = json['front_shiny'];
    frontShinyFemale = json['front_shiny_female'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['front_default'] = frontDefault;
    _data['front_female'] = frontFemale;
    _data['front_shiny'] = frontShiny;
    _data['front_shiny_female'] = frontShinyFemale;
    return _data;
  }
}

class Generation_viii {
  Generation_viii({
    required this.icons,
  });
  late final Icons icons;

  Generation_viii.fromJson(Map<String, dynamic> json) {
    icons = Icons.fromJson(json['icons']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['icons'] = icons.toJson();
    return _data;
  }
}

class Stats {
  Stats({
    required this.baseStat,
    required this.effort,
    required this.stat,
  });
  late final int baseStat;
  late final int effort;
  late final Stat stat;

  Stats.fromJson(Map<String, dynamic> json) {
    baseStat = json['base_stat'];
    effort = json['effort'];
    stat = Stat.fromJson(json['stat']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['base_stat'] = baseStat;
    _data['effort'] = effort;
    _data['stat'] = stat.toJson();
    return _data;
  }
}

class Stat {
  Stat({
    required this.name,
    required this.url,
  });
  late final String name;
  late final String url;

  Stat.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['name'] = name;
    _data['url'] = url;
    return _data;
  }
}

class Types {
  Types({
    required this.slot,
    required this.type,
  });
  late final int slot;
  late final Type type;

  Types.fromJson(Map<String, dynamic> json) {
    slot = json['slot'];
    type = Type.fromJson(json['type']);
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['slot'] = slot;
    _data['type'] = type.toJson();
    return _data;
  }
}

class Type {
  Type({
    required this.name,
    required this.url,
  });
  late final String name;
  late final String url;

  Type.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    url = json['url'];
  }

  Map<String, dynamic> toJson() {
    final _data = <String, dynamic>{};
    _data['name'] = name;
    _data['url'] = url;
    return _data;
  }
}
