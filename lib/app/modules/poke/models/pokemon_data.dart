import 'dart:convert';

Pokemon pokemonFromJson(String str) => Pokemon.fromJson(json.decode(str));

String pokemonToJson(Pokemon data) => json.encode(data.toJson());

class Pokemon {
  final List<Abilities>? abilities;
  final int? baseExperience;
  final List<Forms>? forms;
  final List<GameIndices>? gameIndices;
  final int? height;
  final List<dynamic>? heldItems;
  final int? id;
  final bool? isDefault;
  final String? locationAreaEncounters;
  final List<Moves>? moves;
  final String? name;
  final int? order;
  final List<dynamic>? pastTypes;
  final Species? species;
  final Sprites? sprites;
  final List<Stats>? stats;
  final List<Types>? types;
  final int? weight;

  Pokemon({
    this.abilities,
    this.baseExperience,
    this.forms,
    this.gameIndices,
    this.height,
    this.heldItems,
    this.id,
    this.isDefault,
    this.locationAreaEncounters,
    this.moves,
    this.name,
    this.order,
    this.pastTypes,
    this.species,
    this.sprites,
    this.stats,
    this.types,
    this.weight,
  });

  Pokemon.fromJson(Map<String, dynamic> json)
      : abilities = (json['abilities'] as List?)
            ?.map((dynamic e) => Abilities.fromJson(e as Map<String, dynamic>))
            .toList(),
        baseExperience = json['base_experience'] as int?,
        forms = (json['forms'] as List?)
            ?.map((dynamic e) => Forms.fromJson(e as Map<String, dynamic>))
            .toList(),
        gameIndices = (json['game_indices'] as List?)
            ?.map(
                (dynamic e) => GameIndices.fromJson(e as Map<String, dynamic>))
            .toList(),
        height = json['height'] as int?,
        heldItems = json['held_items'] as List?,
        id = json['id'] as int?,
        isDefault = json['is_default'] as bool?,
        locationAreaEncounters = json['location_area_encounters'] as String?,
        moves = (json['moves'] as List?)
            ?.map((dynamic e) => Moves.fromJson(e as Map<String, dynamic>))
            .toList(),
        name = json['name'] as String?,
        order = json['order'] as int?,
        pastTypes = json['past_types'] as List?,
        species = (json['species'] as Map<String, dynamic>?) != null
            ? Species.fromJson(json['species'] as Map<String, dynamic>)
            : null,
        sprites = (json['sprites'] as Map<String, dynamic>?) != null
            ? Sprites.fromJson(json['sprites'] as Map<String, dynamic>)
            : null,
        stats = (json['stats'] as List?)
            ?.map((dynamic e) => Stats.fromJson(e as Map<String, dynamic>))
            .toList(),
        types = (json['types'] as List?)
            ?.map((dynamic e) => Types.fromJson(e as Map<String, dynamic>))
            .toList(),
        weight = json['weight'] as int?;

  Map<String, dynamic> toJson() => {
        'abilities': abilities?.map((e) => e.toJson()).toList(),
        'base_experience': baseExperience,
        'forms': forms?.map((e) => e.toJson()).toList(),
        'game_indices': gameIndices?.map((e) => e.toJson()).toList(),
        'height': height,
        'held_items': heldItems,
        'id': id,
        'is_default': isDefault,
        'location_area_encounters': locationAreaEncounters,
        'moves': moves?.map((e) => e.toJson()).toList(),
        'name': name,
        'order': order,
        'past_types': pastTypes,
        'species': species?.toJson(),
        'sprites': sprites?.toJson(),
        'stats': stats?.map((e) => e.toJson()).toList(),
        'types': types?.map((e) => e.toJson()).toList(),
        'weight': weight
      };
}

class Abilities {
  final Ability? ability;
  final bool? isHidden;
  final int? slot;

  Abilities({
    this.ability,
    this.isHidden,
    this.slot,
  });

  Abilities.fromJson(Map<String, dynamic> json)
      : ability = (json['ability'] as Map<String, dynamic>?) != null
            ? Ability.fromJson(json['ability'] as Map<String, dynamic>)
            : null,
        isHidden = json['is_hidden'] as bool?,
        slot = json['slot'] as int?;

  Map<String, dynamic> toJson() =>
      {'ability': ability?.toJson(), 'is_hidden': isHidden, 'slot': slot};
}

class Ability {
  final String? name;
  final String? url;

  Ability({
    this.name,
    this.url,
  });

  Ability.fromJson(Map<String, dynamic> json)
      : name = json['name'] as String?,
        url = json['url'] as String?;

  Map<String, dynamic> toJson() => {'name': name, 'url': url};
}

class Forms {
  final String? name;
  final String? url;

  Forms({
    this.name,
    this.url,
  });

  Forms.fromJson(Map<String, dynamic> json)
      : name = json['name'] as String?,
        url = json['url'] as String?;

  Map<String, dynamic> toJson() => {'name': name, 'url': url};
}

class GameIndices {
  final int? gameIndex;
  final Version? version;

  GameIndices({
    this.gameIndex,
    this.version,
  });

  GameIndices.fromJson(Map<String, dynamic> json)
      : gameIndex = json['game_index'] as int?,
        version = (json['version'] as Map<String, dynamic>?) != null
            ? Version.fromJson(json['version'] as Map<String, dynamic>)
            : null;

  Map<String, dynamic> toJson() =>
      {'game_index': gameIndex, 'version': version?.toJson()};
}

class Version {
  final String? name;
  final String? url;

  Version({
    this.name,
    this.url,
  });

  Version.fromJson(Map<String, dynamic> json)
      : name = json['name'] as String?,
        url = json['url'] as String?;

  Map<String, dynamic> toJson() => {'name': name, 'url': url};
}

class Moves {
  final Move? move;
  final List<VersionGroupDetails>? versionGroupDetails;

  Moves({
    this.move,
    this.versionGroupDetails,
  });

  Moves.fromJson(Map<String, dynamic> json)
      : move = (json['move'] as Map<String, dynamic>?) != null
            ? Move.fromJson(json['move'] as Map<String, dynamic>)
            : null,
        versionGroupDetails = (json['version_group_details'] as List?)
            ?.map((dynamic e) =>
                VersionGroupDetails.fromJson(e as Map<String, dynamic>))
            .toList();

  Map<String, dynamic> toJson() => {
        'move': move?.toJson(),
        'version_group_details':
            versionGroupDetails?.map((e) => e.toJson()).toList()
      };
}

class Move {
  final String? name;
  final String? url;

  Move({
    this.name,
    this.url,
  });

  Move.fromJson(Map<String, dynamic> json)
      : name = json['name'] as String?,
        url = json['url'] as String?;

  Map<String, dynamic> toJson() => {'name': name, 'url': url};
}

class VersionGroupDetails {
  final int? levelLearnedAt;
  final MoveLearnMethod? moveLearnMethod;
  final VersionGroup? versionGroup;

  VersionGroupDetails({
    this.levelLearnedAt,
    this.moveLearnMethod,
    this.versionGroup,
  });

  VersionGroupDetails.fromJson(Map<String, dynamic> json)
      : levelLearnedAt = json['level_learned_at'] as int?,
        moveLearnMethod =
            (json['move_learn_method'] as Map<String, dynamic>?) != null
                ? MoveLearnMethod.fromJson(
                    json['move_learn_method'] as Map<String, dynamic>)
                : null,
        versionGroup = (json['version_group'] as Map<String, dynamic>?) != null
            ? VersionGroup.fromJson(
                json['version_group'] as Map<String, dynamic>)
            : null;

  Map<String, dynamic> toJson() => {
        'level_learned_at': levelLearnedAt,
        'move_learn_method': moveLearnMethod?.toJson(),
        'version_group': versionGroup?.toJson()
      };
}

class MoveLearnMethod {
  final String? name;
  final String? url;

  MoveLearnMethod({
    this.name,
    this.url,
  });

  MoveLearnMethod.fromJson(Map<String, dynamic> json)
      : name = json['name'] as String?,
        url = json['url'] as String?;

  Map<String, dynamic> toJson() => {'name': name, 'url': url};
}

class VersionGroup {
  final String? name;
  final String? url;

  VersionGroup({
    this.name,
    this.url,
  });

  VersionGroup.fromJson(Map<String, dynamic> json)
      : name = json['name'] as String?,
        url = json['url'] as String?;

  Map<String, dynamic> toJson() => {'name': name, 'url': url};
}

class Species {
  final String? name;
  final String? url;

  Species({
    this.name,
    this.url,
  });

  Species.fromJson(Map<String, dynamic> json)
      : name = json['name'] as String?,
        url = json['url'] as String?;

  Map<String, dynamic> toJson() => {'name': name, 'url': url};
}

class Sprites {
  final String? backDefault;
  final dynamic backFemale;
  final String? backShiny;
  final dynamic backShinyFemale;
  final String? frontDefault;
  final dynamic frontFemale;
  final String? frontShiny;
  final dynamic frontShinyFemale;
  final Other? other;
  final Versions? versions;

  Sprites({
    this.backDefault,
    this.backFemale,
    this.backShiny,
    this.backShinyFemale,
    this.frontDefault,
    this.frontFemale,
    this.frontShiny,
    this.frontShinyFemale,
    this.other,
    this.versions,
  });

  Sprites.fromJson(Map<String, dynamic> json)
      : backDefault = json['back_default'] as String?,
        backFemale = json['back_female'],
        backShiny = json['back_shiny'] as String?,
        backShinyFemale = json['back_shiny_female'],
        frontDefault = json['front_default'] as String?,
        frontFemale = json['front_female'],
        frontShiny = json['front_shiny'] as String?,
        frontShinyFemale = json['front_shiny_female'],
        other = (json['other'] as Map<String, dynamic>?) != null
            ? Other.fromJson(json['other'] as Map<String, dynamic>)
            : null,
        versions = (json['versions'] as Map<String, dynamic>?) != null
            ? Versions.fromJson(json['versions'] as Map<String, dynamic>)
            : null;

  Map<String, dynamic> toJson() => {
        'back_default': backDefault,
        'back_female': backFemale,
        'back_shiny': backShiny,
        'back_shiny_female': backShinyFemale,
        'front_default': frontDefault,
        'front_female': frontFemale,
        'front_shiny': frontShiny,
        'front_shiny_female': frontShinyFemale,
        'other': other?.toJson(),
        'versions': versions?.toJson()
      };
}

class Other {
  final DreamWorld? dreamWorld;
  final Home? home;
  final Official_artwork? official_artwork;

  Other({
    this.dreamWorld,
    this.home,
    this.official_artwork,
  });

  Other.fromJson(Map<String, dynamic> json)
      : dreamWorld = (json['dream_world'] as Map<String, dynamic>?) != null
            ? DreamWorld.fromJson(json['dream_world'] as Map<String, dynamic>)
            : null,
        home = (json['home'] as Map<String, dynamic>?) != null
            ? Home.fromJson(json['home'] as Map<String, dynamic>)
            : null,
        official_artwork =
            (json['official-artwork'] as Map<String, dynamic>?) != null
                ? Official_artwork.fromJson(
                    json['official-artwork'] as Map<String, dynamic>)
                : null;

  Map<String, dynamic> toJson() => {
        'dream_world': dreamWorld?.toJson(),
        'home': home?.toJson(),
        'official-artwork': official_artwork?.toJson()
      };
}

class DreamWorld {
  final String? frontDefault;
  final dynamic frontFemale;

  DreamWorld({
    this.frontDefault,
    this.frontFemale,
  });

  DreamWorld.fromJson(Map<String, dynamic> json)
      : frontDefault = json['front_default'] as String?,
        frontFemale = json['front_female'];

  Map<String, dynamic> toJson() =>
      {'front_default': frontDefault, 'front_female': frontFemale};
}

class Home {
  final String? frontDefault;
  final dynamic frontFemale;
  final String? frontShiny;
  final dynamic frontShinyFemale;

  Home({
    this.frontDefault,
    this.frontFemale,
    this.frontShiny,
    this.frontShinyFemale,
  });

  Home.fromJson(Map<String, dynamic> json)
      : frontDefault = json['front_default'] as String?,
        frontFemale = json['front_female'],
        frontShiny = json['front_shiny'] as String?,
        frontShinyFemale = json['front_shiny_female'];

  Map<String, dynamic> toJson() => {
        'front_default': frontDefault,
        'front_female': frontFemale,
        'front_shiny': frontShiny,
        'front_shiny_female': frontShinyFemale
      };
}

class Official_artwork {
  final String? frontDefault;

  Official_artwork({
    this.frontDefault,
  });

  Official_artwork.fromJson(Map<String, dynamic> json)
      : frontDefault = json['front_default'] as String?;

  Map<String, dynamic> toJson() => {'front_default': frontDefault};
}

class Versions {
  final Generation_i? generation_i;
  final Generation_ii? generation_ii;
  final Generation_iii? generation_iii;
  final Generation_iv? generation_iv;
  final Generation_v? generation_v;
  final Generation_vi? generation_vi;
  final Generation_vii? generation_vii;
  final Generation_viii? generation_viii;

  Versions({
    this.generation_i,
    this.generation_ii,
    this.generation_iii,
    this.generation_iv,
    this.generation_v,
    this.generation_vi,
    this.generation_vii,
    this.generation_viii,
  });

  Versions.fromJson(Map<String, dynamic> json)
      : generation_i = (json['generation-i'] as Map<String, dynamic>?) != null
            ? Generation_i.fromJson(
                json['generation-i'] as Map<String, dynamic>)
            : null,
        generation_ii = (json['generation-ii'] as Map<String, dynamic>?) != null
            ? Generation_ii.fromJson(
                json['generation-ii'] as Map<String, dynamic>)
            : null,
        generation_iii =
            (json['generation-iii'] as Map<String, dynamic>?) != null
                ? Generation_iii.fromJson(
                    json['generation-iii'] as Map<String, dynamic>)
                : null,
        generation_iv = (json['generation-iv'] as Map<String, dynamic>?) != null
            ? Generation_iv.fromJson(
                json['generation-iv'] as Map<String, dynamic>)
            : null,
        generation_v = (json['generation-v'] as Map<String, dynamic>?) != null
            ? Generation_v.fromJson(
                json['generation-v'] as Map<String, dynamic>)
            : null,
        generation_vi = (json['generation-vi'] as Map<String, dynamic>?) != null
            ? Generation_vi.fromJson(
                json['generation-vi'] as Map<String, dynamic>)
            : null,
        generation_vii =
            (json['generation-vii'] as Map<String, dynamic>?) != null
                ? Generation_vii.fromJson(
                    json['generation-vii'] as Map<String, dynamic>)
                : null,
        generation_viii =
            (json['generation-viii'] as Map<String, dynamic>?) != null
                ? Generation_viii.fromJson(
                    json['generation-viii'] as Map<String, dynamic>)
                : null;

  Map<String, dynamic> toJson() => {
        'generation-i': generation_i?.toJson(),
        'generation-ii': generation_ii?.toJson(),
        'generation-iii': generation_iii?.toJson(),
        'generation-iv': generation_iv?.toJson(),
        'generation-v': generation_v?.toJson(),
        'generation-vi': generation_vi?.toJson(),
        'generation-vii': generation_vii?.toJson(),
        'generation-viii': generation_viii?.toJson()
      };
}

class Generation_i {
  final Red_blue? red_blue;
  final Yellow? yellow;

  Generation_i({
    this.red_blue,
    this.yellow,
  });

  Generation_i.fromJson(Map<String, dynamic> json)
      : red_blue = (json['red-blue'] as Map<String, dynamic>?) != null
            ? Red_blue.fromJson(json['red-blue'] as Map<String, dynamic>)
            : null,
        yellow = (json['yellow'] as Map<String, dynamic>?) != null
            ? Yellow.fromJson(json['yellow'] as Map<String, dynamic>)
            : null;

  Map<String, dynamic> toJson() =>
      {'red-blue': red_blue?.toJson(), 'yellow': yellow?.toJson()};
}

class Red_blue {
  final String? backDefault;
  final String? backGray;
  final String? backTransparent;
  final String? frontDefault;
  final String? frontGray;
  final String? frontTransparent;

  Red_blue({
    this.backDefault,
    this.backGray,
    this.backTransparent,
    this.frontDefault,
    this.frontGray,
    this.frontTransparent,
  });

  Red_blue.fromJson(Map<String, dynamic> json)
      : backDefault = json['back_default'] as String?,
        backGray = json['back_gray'] as String?,
        backTransparent = json['back_transparent'] as String?,
        frontDefault = json['front_default'] as String?,
        frontGray = json['front_gray'] as String?,
        frontTransparent = json['front_transparent'] as String?;

  Map<String, dynamic> toJson() => {
        'back_default': backDefault,
        'back_gray': backGray,
        'back_transparent': backTransparent,
        'front_default': frontDefault,
        'front_gray': frontGray,
        'front_transparent': frontTransparent
      };
}

class Yellow {
  final String? backDefault;
  final String? backGray;
  final String? backTransparent;
  final String? frontDefault;
  final String? frontGray;
  final String? frontTransparent;

  Yellow({
    this.backDefault,
    this.backGray,
    this.backTransparent,
    this.frontDefault,
    this.frontGray,
    this.frontTransparent,
  });

  Yellow.fromJson(Map<String, dynamic> json)
      : backDefault = json['back_default'] as String?,
        backGray = json['back_gray'] as String?,
        backTransparent = json['back_transparent'] as String?,
        frontDefault = json['front_default'] as String?,
        frontGray = json['front_gray'] as String?,
        frontTransparent = json['front_transparent'] as String?;

  Map<String, dynamic> toJson() => {
        'back_default': backDefault,
        'back_gray': backGray,
        'back_transparent': backTransparent,
        'front_default': frontDefault,
        'front_gray': frontGray,
        'front_transparent': frontTransparent
      };
}

class Generation_ii {
  final Crystal? crystal;
  final Gold? gold;
  final Silver? silver;

  Generation_ii({
    this.crystal,
    this.gold,
    this.silver,
  });

  Generation_ii.fromJson(Map<String, dynamic> json)
      : crystal = (json['crystal'] as Map<String, dynamic>?) != null
            ? Crystal.fromJson(json['crystal'] as Map<String, dynamic>)
            : null,
        gold = (json['gold'] as Map<String, dynamic>?) != null
            ? Gold.fromJson(json['gold'] as Map<String, dynamic>)
            : null,
        silver = (json['silver'] as Map<String, dynamic>?) != null
            ? Silver.fromJson(json['silver'] as Map<String, dynamic>)
            : null;

  Map<String, dynamic> toJson() => {
        'crystal': crystal?.toJson(),
        'gold': gold?.toJson(),
        'silver': silver?.toJson()
      };
}

class Crystal {
  final String? backDefault;
  final String? backShiny;
  final String? backShinyTransparent;
  final String? backTransparent;
  final String? frontDefault;
  final String? frontShiny;
  final String? frontShinyTransparent;
  final String? frontTransparent;

  Crystal({
    this.backDefault,
    this.backShiny,
    this.backShinyTransparent,
    this.backTransparent,
    this.frontDefault,
    this.frontShiny,
    this.frontShinyTransparent,
    this.frontTransparent,
  });

  Crystal.fromJson(Map<String, dynamic> json)
      : backDefault = json['back_default'] as String?,
        backShiny = json['back_shiny'] as String?,
        backShinyTransparent = json['back_shiny_transparent'] as String?,
        backTransparent = json['back_transparent'] as String?,
        frontDefault = json['front_default'] as String?,
        frontShiny = json['front_shiny'] as String?,
        frontShinyTransparent = json['front_shiny_transparent'] as String?,
        frontTransparent = json['front_transparent'] as String?;

  Map<String, dynamic> toJson() => {
        'back_default': backDefault,
        'back_shiny': backShiny,
        'back_shiny_transparent': backShinyTransparent,
        'back_transparent': backTransparent,
        'front_default': frontDefault,
        'front_shiny': frontShiny,
        'front_shiny_transparent': frontShinyTransparent,
        'front_transparent': frontTransparent
      };
}

class Gold {
  final String? backDefault;
  final String? backShiny;
  final String? frontDefault;
  final String? frontShiny;
  final String? frontTransparent;

  Gold({
    this.backDefault,
    this.backShiny,
    this.frontDefault,
    this.frontShiny,
    this.frontTransparent,
  });

  Gold.fromJson(Map<String, dynamic> json)
      : backDefault = json['back_default'] as String?,
        backShiny = json['back_shiny'] as String?,
        frontDefault = json['front_default'] as String?,
        frontShiny = json['front_shiny'] as String?,
        frontTransparent = json['front_transparent'] as String?;

  Map<String, dynamic> toJson() => {
        'back_default': backDefault,
        'back_shiny': backShiny,
        'front_default': frontDefault,
        'front_shiny': frontShiny,
        'front_transparent': frontTransparent
      };
}

class Silver {
  final String? backDefault;
  final String? backShiny;
  final String? frontDefault;
  final String? frontShiny;
  final String? frontTransparent;

  Silver({
    this.backDefault,
    this.backShiny,
    this.frontDefault,
    this.frontShiny,
    this.frontTransparent,
  });

  Silver.fromJson(Map<String, dynamic> json)
      : backDefault = json['back_default'] as String?,
        backShiny = json['back_shiny'] as String?,
        frontDefault = json['front_default'] as String?,
        frontShiny = json['front_shiny'] as String?,
        frontTransparent = json['front_transparent'] as String?;

  Map<String, dynamic> toJson() => {
        'back_default': backDefault,
        'back_shiny': backShiny,
        'front_default': frontDefault,
        'front_shiny': frontShiny,
        'front_transparent': frontTransparent
      };
}

class Generation_iii {
  final Emerald? emerald;
  final Firered_leafgreen? firered_leafgreen;
  final Ruby_sapphire? ruby_sapphire;

  Generation_iii({
    this.emerald,
    this.firered_leafgreen,
    this.ruby_sapphire,
  });

  Generation_iii.fromJson(Map<String, dynamic> json)
      : emerald = (json['emerald'] as Map<String, dynamic>?) != null
            ? Emerald.fromJson(json['emerald'] as Map<String, dynamic>)
            : null,
        firered_leafgreen =
            (json['firered-leafgreen'] as Map<String, dynamic>?) != null
                ? Firered_leafgreen.fromJson(
                    json['firered-leafgreen'] as Map<String, dynamic>)
                : null,
        ruby_sapphire = (json['ruby-sapphire'] as Map<String, dynamic>?) != null
            ? Ruby_sapphire.fromJson(
                json['ruby-sapphire'] as Map<String, dynamic>)
            : null;

  Map<String, dynamic> toJson() => {
        'emerald': emerald?.toJson(),
        'firered-leafgreen': firered_leafgreen?.toJson(),
        'ruby-sapphire': ruby_sapphire?.toJson()
      };
}

class Emerald {
  final String? frontDefault;
  final String? frontShiny;

  Emerald({
    this.frontDefault,
    this.frontShiny,
  });

  Emerald.fromJson(Map<String, dynamic> json)
      : frontDefault = json['front_default'] as String?,
        frontShiny = json['front_shiny'] as String?;

  Map<String, dynamic> toJson() =>
      {'front_default': frontDefault, 'front_shiny': frontShiny};
}

class Firered_leafgreen {
  final String? backDefault;
  final String? backShiny;
  final String? frontDefault;
  final String? frontShiny;

  Firered_leafgreen({
    this.backDefault,
    this.backShiny,
    this.frontDefault,
    this.frontShiny,
  });

  Firered_leafgreen.fromJson(Map<String, dynamic> json)
      : backDefault = json['back_default'] as String?,
        backShiny = json['back_shiny'] as String?,
        frontDefault = json['front_default'] as String?,
        frontShiny = json['front_shiny'] as String?;

  Map<String, dynamic> toJson() => {
        'back_default': backDefault,
        'back_shiny': backShiny,
        'front_default': frontDefault,
        'front_shiny': frontShiny
      };
}

class Ruby_sapphire {
  final String? backDefault;
  final String? backShiny;
  final String? frontDefault;
  final String? frontShiny;

  Ruby_sapphire({
    this.backDefault,
    this.backShiny,
    this.frontDefault,
    this.frontShiny,
  });

  Ruby_sapphire.fromJson(Map<String, dynamic> json)
      : backDefault = json['back_default'] as String?,
        backShiny = json['back_shiny'] as String?,
        frontDefault = json['front_default'] as String?,
        frontShiny = json['front_shiny'] as String?;

  Map<String, dynamic> toJson() => {
        'back_default': backDefault,
        'back_shiny': backShiny,
        'front_default': frontDefault,
        'front_shiny': frontShiny
      };
}

class Generation_iv {
  final Diamond_pearl? diamond_pearl;
  final Heartgold_soulsilver? heartgold_soulsilver;
  final Platinum? platinum;

  Generation_iv({
    this.diamond_pearl,
    this.heartgold_soulsilver,
    this.platinum,
  });

  Generation_iv.fromJson(Map<String, dynamic> json)
      : diamond_pearl = (json['diamond-pearl'] as Map<String, dynamic>?) != null
            ? Diamond_pearl.fromJson(
                json['diamond-pearl'] as Map<String, dynamic>)
            : null,
        heartgold_soulsilver =
            (json['heartgold-soulsilver'] as Map<String, dynamic>?) != null
                ? Heartgold_soulsilver.fromJson(
                    json['heartgold-soulsilver'] as Map<String, dynamic>)
                : null,
        platinum = (json['platinum'] as Map<String, dynamic>?) != null
            ? Platinum.fromJson(json['platinum'] as Map<String, dynamic>)
            : null;

  Map<String, dynamic> toJson() => {
        'diamond-pearl': diamond_pearl?.toJson(),
        'heartgold-soulsilver': heartgold_soulsilver?.toJson(),
        'platinum': platinum?.toJson()
      };
}

class Diamond_pearl {
  final String? backDefault;
  final dynamic backFemale;
  final String? backShiny;
  final dynamic backShinyFemale;
  final String? frontDefault;
  final dynamic frontFemale;
  final String? frontShiny;
  final dynamic frontShinyFemale;

  Diamond_pearl({
    this.backDefault,
    this.backFemale,
    this.backShiny,
    this.backShinyFemale,
    this.frontDefault,
    this.frontFemale,
    this.frontShiny,
    this.frontShinyFemale,
  });

  Diamond_pearl.fromJson(Map<String, dynamic> json)
      : backDefault = json['back_default'] as String?,
        backFemale = json['back_female'],
        backShiny = json['back_shiny'] as String?,
        backShinyFemale = json['back_shiny_female'],
        frontDefault = json['front_default'] as String?,
        frontFemale = json['front_female'],
        frontShiny = json['front_shiny'] as String?,
        frontShinyFemale = json['front_shiny_female'];

  Map<String, dynamic> toJson() => {
        'back_default': backDefault,
        'back_female': backFemale,
        'back_shiny': backShiny,
        'back_shiny_female': backShinyFemale,
        'front_default': frontDefault,
        'front_female': frontFemale,
        'front_shiny': frontShiny,
        'front_shiny_female': frontShinyFemale
      };
}

class Heartgold_soulsilver {
  final String? backDefault;
  final dynamic backFemale;
  final String? backShiny;
  final dynamic backShinyFemale;
  final String? frontDefault;
  final dynamic frontFemale;
  final String? frontShiny;
  final dynamic frontShinyFemale;

  Heartgold_soulsilver({
    this.backDefault,
    this.backFemale,
    this.backShiny,
    this.backShinyFemale,
    this.frontDefault,
    this.frontFemale,
    this.frontShiny,
    this.frontShinyFemale,
  });

  Heartgold_soulsilver.fromJson(Map<String, dynamic> json)
      : backDefault = json['back_default'] as String?,
        backFemale = json['back_female'],
        backShiny = json['back_shiny'] as String?,
        backShinyFemale = json['back_shiny_female'],
        frontDefault = json['front_default'] as String?,
        frontFemale = json['front_female'],
        frontShiny = json['front_shiny'] as String?,
        frontShinyFemale = json['front_shiny_female'];

  Map<String, dynamic> toJson() => {
        'back_default': backDefault,
        'back_female': backFemale,
        'back_shiny': backShiny,
        'back_shiny_female': backShinyFemale,
        'front_default': frontDefault,
        'front_female': frontFemale,
        'front_shiny': frontShiny,
        'front_shiny_female': frontShinyFemale
      };
}

class Platinum {
  final String? backDefault;
  final dynamic backFemale;
  final String? backShiny;
  final dynamic backShinyFemale;
  final String? frontDefault;
  final dynamic frontFemale;
  final String? frontShiny;
  final dynamic frontShinyFemale;

  Platinum({
    this.backDefault,
    this.backFemale,
    this.backShiny,
    this.backShinyFemale,
    this.frontDefault,
    this.frontFemale,
    this.frontShiny,
    this.frontShinyFemale,
  });

  Platinum.fromJson(Map<String, dynamic> json)
      : backDefault = json['back_default'] as String?,
        backFemale = json['back_female'],
        backShiny = json['back_shiny'] as String?,
        backShinyFemale = json['back_shiny_female'],
        frontDefault = json['front_default'] as String?,
        frontFemale = json['front_female'],
        frontShiny = json['front_shiny'] as String?,
        frontShinyFemale = json['front_shiny_female'];

  Map<String, dynamic> toJson() => {
        'back_default': backDefault,
        'back_female': backFemale,
        'back_shiny': backShiny,
        'back_shiny_female': backShinyFemale,
        'front_default': frontDefault,
        'front_female': frontFemale,
        'front_shiny': frontShiny,
        'front_shiny_female': frontShinyFemale
      };
}

class Generation_v {
  final Black_white? black_white;

  Generation_v({
    this.black_white,
  });

  Generation_v.fromJson(Map<String, dynamic> json)
      : black_white = (json['black-white'] as Map<String, dynamic>?) != null
            ? Black_white.fromJson(json['black-white'] as Map<String, dynamic>)
            : null;

  Map<String, dynamic> toJson() => {'black-white': black_white?.toJson()};
}

class Black_white {
  final Animated? animated;
  final String? backDefault;
  final dynamic backFemale;
  final String? backShiny;
  final dynamic backShinyFemale;
  final String? frontDefault;
  final dynamic frontFemale;
  final String? frontShiny;
  final dynamic frontShinyFemale;

  Black_white({
    this.animated,
    this.backDefault,
    this.backFemale,
    this.backShiny,
    this.backShinyFemale,
    this.frontDefault,
    this.frontFemale,
    this.frontShiny,
    this.frontShinyFemale,
  });

  Black_white.fromJson(Map<String, dynamic> json)
      : animated = (json['animated'] as Map<String, dynamic>?) != null
            ? Animated.fromJson(json['animated'] as Map<String, dynamic>)
            : null,
        backDefault = json['back_default'] as String?,
        backFemale = json['back_female'],
        backShiny = json['back_shiny'] as String?,
        backShinyFemale = json['back_shiny_female'],
        frontDefault = json['front_default'] as String?,
        frontFemale = json['front_female'],
        frontShiny = json['front_shiny'] as String?,
        frontShinyFemale = json['front_shiny_female'];

  Map<String, dynamic> toJson() => {
        'animated': animated?.toJson(),
        'back_default': backDefault,
        'back_female': backFemale,
        'back_shiny': backShiny,
        'back_shiny_female': backShinyFemale,
        'front_default': frontDefault,
        'front_female': frontFemale,
        'front_shiny': frontShiny,
        'front_shiny_female': frontShinyFemale
      };
}

class Animated {
  final String? backDefault;
  final dynamic backFemale;
  final String? backShiny;
  final dynamic backShinyFemale;
  final String? frontDefault;
  final dynamic frontFemale;
  final String? frontShiny;
  final dynamic frontShinyFemale;

  Animated({
    this.backDefault,
    this.backFemale,
    this.backShiny,
    this.backShinyFemale,
    this.frontDefault,
    this.frontFemale,
    this.frontShiny,
    this.frontShinyFemale,
  });

  Animated.fromJson(Map<String, dynamic> json)
      : backDefault = json['back_default'] as String?,
        backFemale = json['back_female'],
        backShiny = json['back_shiny'] as String?,
        backShinyFemale = json['back_shiny_female'],
        frontDefault = json['front_default'] as String?,
        frontFemale = json['front_female'],
        frontShiny = json['front_shiny'] as String?,
        frontShinyFemale = json['front_shiny_female'];

  Map<String, dynamic> toJson() => {
        'back_default': backDefault,
        'back_female': backFemale,
        'back_shiny': backShiny,
        'back_shiny_female': backShinyFemale,
        'front_default': frontDefault,
        'front_female': frontFemale,
        'front_shiny': frontShiny,
        'front_shiny_female': frontShinyFemale
      };
}

class Generation_vi {
  final Omegaruby_alphasapphire? omegaruby_alphasapphire;
  final X_y? x_y;

  Generation_vi({
    this.omegaruby_alphasapphire,
    this.x_y,
  });

  Generation_vi.fromJson(Map<String, dynamic> json)
      : omegaruby_alphasapphire =
            (json['omegaruby-alphasapphire'] as Map<String, dynamic>?) != null
                ? Omegaruby_alphasapphire.fromJson(
                    json['omegaruby-alphasapphire'] as Map<String, dynamic>)
                : null,
        x_y = (json['x-y'] as Map<String, dynamic>?) != null
            ? X_y.fromJson(json['x-y'] as Map<String, dynamic>)
            : null;

  Map<String, dynamic> toJson() => {
        'omegaruby-alphasapphire': omegaruby_alphasapphire?.toJson(),
        'x-y': x_y?.toJson()
      };
}

class Omegaruby_alphasapphire {
  final String? frontDefault;
  final dynamic frontFemale;
  final String? frontShiny;
  final dynamic frontShinyFemale;

  Omegaruby_alphasapphire({
    this.frontDefault,
    this.frontFemale,
    this.frontShiny,
    this.frontShinyFemale,
  });

  Omegaruby_alphasapphire.fromJson(Map<String, dynamic> json)
      : frontDefault = json['front_default'] as String?,
        frontFemale = json['front_female'],
        frontShiny = json['front_shiny'] as String?,
        frontShinyFemale = json['front_shiny_female'];

  Map<String, dynamic> toJson() => {
        'front_default': frontDefault,
        'front_female': frontFemale,
        'front_shiny': frontShiny,
        'front_shiny_female': frontShinyFemale
      };
}

class X_y {
  final String? frontDefault;
  final dynamic frontFemale;
  final String? frontShiny;
  final dynamic frontShinyFemale;

  X_y({
    this.frontDefault,
    this.frontFemale,
    this.frontShiny,
    this.frontShinyFemale,
  });

  X_y.fromJson(Map<String, dynamic> json)
      : frontDefault = json['front_default'] as String?,
        frontFemale = json['front_female'],
        frontShiny = json['front_shiny'] as String?,
        frontShinyFemale = json['front_shiny_female'];

  Map<String, dynamic> toJson() => {
        'front_default': frontDefault,
        'front_female': frontFemale,
        'front_shiny': frontShiny,
        'front_shiny_female': frontShinyFemale
      };
}

class Generation_vii {
  final PokeIcons? icons;
  final Ultra_sun_ultra_moon? ultra_sun_ultra_moon;

  Generation_vii({
    this.icons,
    this.ultra_sun_ultra_moon,
  });

  Generation_vii.fromJson(Map<String, dynamic> json)
      : icons = (json['icons'] as Map<String, dynamic>?) != null
            ? PokeIcons.fromJson(json['icons'] as Map<String, dynamic>)
            : null,
        ultra_sun_ultra_moon =
            (json['ultra-sun-ultra-moon'] as Map<String, dynamic>?) != null
                ? Ultra_sun_ultra_moon.fromJson(
                    json['ultra-sun-ultra-moon'] as Map<String, dynamic>)
                : null;

  Map<String, dynamic> toJson() => {
        'icons': icons?.toJson(),
        'ultra-sun-ultra-moon': ultra_sun_ultra_moon?.toJson()
      };
}

class PokeIcons {
  final String? frontDefault;
  final dynamic frontFemale;

  PokeIcons({
    this.frontDefault,
    this.frontFemale,
  });

  PokeIcons.fromJson(Map<String, dynamic> json)
      : frontDefault = json['front_default'] as String?,
        frontFemale = json['front_female'];

  Map<String, dynamic> toJson() =>
      {'front_default': frontDefault, 'front_female': frontFemale};
}

class Ultra_sun_ultra_moon {
  final String? frontDefault;
  final dynamic frontFemale;
  final String? frontShiny;
  final dynamic frontShinyFemale;

  Ultra_sun_ultra_moon({
    this.frontDefault,
    this.frontFemale,
    this.frontShiny,
    this.frontShinyFemale,
  });

  Ultra_sun_ultra_moon.fromJson(Map<String, dynamic> json)
      : frontDefault = json['front_default'] as String?,
        frontFemale = json['front_female'],
        frontShiny = json['front_shiny'] as String?,
        frontShinyFemale = json['front_shiny_female'];

  Map<String, dynamic> toJson() => {
        'front_default': frontDefault,
        'front_female': frontFemale,
        'front_shiny': frontShiny,
        'front_shiny_female': frontShinyFemale
      };
}

class Generation_viii {
  final PokeIcons? icons;

  Generation_viii({
    this.icons,
  });

  Generation_viii.fromJson(Map<String, dynamic> json)
      : icons = (json['icons'] as Map<String, dynamic>?) != null
            ? PokeIcons.fromJson(json['icons'] as Map<String, dynamic>)
            : null;

  Map<String, dynamic> toJson() => {'icons': icons?.toJson()};
}

// class PokeIcons {
//   final String? frontDefault;
//   final dynamic frontFemale;

//   PokeIcons({
//     this.frontDefault,
//     this.frontFemale,
//   });

//   PokeIcons.fromJson(Map<String, dynamic> json)
//       : frontDefault = json['front_default'] as String?,
//         frontFemale = json['front_female'];

//   Map<String, dynamic> toJson() =>
//       {'front_default': frontDefault, 'front_female': frontFemale};
// }

class Stats {
  final int? baseStat;
  final int? effort;
  final Stat? stat;

  Stats({
    this.baseStat,
    this.effort,
    this.stat,
  });

  Stats.fromJson(Map<String, dynamic> json)
      : baseStat = json['base_stat'] as int?,
        effort = json['effort'] as int?,
        stat = (json['stat'] as Map<String, dynamic>?) != null
            ? Stat.fromJson(json['stat'] as Map<String, dynamic>)
            : null;

  Map<String, dynamic> toJson() =>
      {'base_stat': baseStat, 'effort': effort, 'stat': stat?.toJson()};
}

class Stat {
  final String? name;
  final String? url;

  Stat({
    this.name,
    this.url,
  });

  Stat.fromJson(Map<String, dynamic> json)
      : name = json['name'] as String?,
        url = json['url'] as String?;

  Map<String, dynamic> toJson() => {'name': name, 'url': url};
}

class Types {
  final int? slot;
  final Type? type;

  Types({
    this.slot,
    this.type,
  });

  Types.fromJson(Map<String, dynamic> json)
      : slot = json['slot'] as int?,
        type = (json['type'] as Map<String, dynamic>?) != null
            ? Type.fromJson(json['type'] as Map<String, dynamic>)
            : null;

  Map<String, dynamic> toJson() => {'slot': slot, 'type': type?.toJson()};
}

class Type {
  final String? name;
  final String? url;

  Type({
    this.name,
    this.url,
  });

  Type.fromJson(Map<String, dynamic> json)
      : name = json['name'] as String?,
        url = json['url'] as String?;

  Map<String, dynamic> toJson() => {'name': name, 'url': url};
}
