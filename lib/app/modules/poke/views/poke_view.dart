import 'package:fireflutter/app/modules/poke/models/pokemon_data.dart';
import 'package:fireflutter/app/routes/app_pages.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/poke_controller.dart';

class PokeView extends GetView<PokeController> {
  final PokeController con = Get.put(PokeController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.transparent,
        appBar: AppBar(
          title: Text('Pokemon List', style: TextStyle(fontSize: 20)),
          toolbarHeight: 50,
        ),
        body: Center(
          child: Obx(
            () => con.pokemonList.length == 0
                ? CircularProgressIndicator()
                : ListView.builder(
                    itemCount: con.pokemonList.length,
                    itemBuilder: (BuildContext context, int index) {
                      return (index + 1) == con.pokemonList.length
                          ? ElevatedButton.icon(
                              onPressed: () {
                                // print(con.pokeURLList);
                                con.fetchMorePokemon(con.pokeURLList.last.next);
                              },
                              icon: Icon(Icons.arrow_drop_down),
                              label: Text("Load more"),
                              style: ElevatedButton.styleFrom(
                                  minimumSize: Size(double.infinity, 100)),
                            )
                          : Padding(
                              padding: const EdgeInsets.fromLTRB(0, 2, 0, 2),
                              child: Row(
                                children: [buildBtn(con.pokemonList[index])],
                              ),
                            );
                    },
                  ),
          ),
        ));
  }
}

Expanded buildBtn(Pokemon poke) {
  final PokeController con = Get.put(PokeController());
  return Expanded(
    child: DecoratedBox(
      decoration: BoxDecoration(
          gradient: buildGradient(
              buildColor(poke.types![0].type!.name ?? "0"),
              poke.types!.length > 1
                  ? buildColor(poke.types![1].type!.name ?? "0")
                  : buildColor(poke.types![0].type!.name ?? "0"))),
      child: ElevatedButton(
        child: Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
          Text("#${poke.id}"),
          Image.network(
            poke.sprites!.other!.official_artwork!.frontDefault ??
                "https://via.placeholder.com/150",
            height: 130,
          ),
          poke.types!.length > 1
              ? Column(
                  children: [
                    Text(
                      poke.name!.capitalize!,
                      style: TextStyle(color: Colors.white, fontSize: 15),
                    ),
                    Row(
                      children: [
                        buildChip(buildColor(poke.types![0].type!.name ?? "0"),
                            poke.types![0].type!.name ?? "0"),
                        SizedBox(
                          width: 5,
                        ),
                        buildChip(buildColor(poke.types![1].type!.name ?? "0"),
                            poke.types![1].type!.name ?? "0")
                      ],
                    )
                  ],
                )
              : Column(
                  children: [
                    Text(
                      poke.name!.capitalize!,
                      style: TextStyle(color: Colors.white, fontSize: 15),
                    ),
                    buildChip(buildColor(poke.types![0].type!.name ?? "0"),
                        poke.types![0].type!.name ?? "0"),
                  ],
                ),
        ]),
        onPressed: () {
          con.currentPoke.add(poke);
          Get.toNamed(Routes.POKE_DETAIL);
        },
        style: ElevatedButton.styleFrom(primary: Colors.transparent),
      ),
    ),
  );
}

Chip buildChip(Color color, String type) {
  return Chip(
    label: Text(type,
        style: TextStyle(
          color: Colors.white,
        )),
    backgroundColor: color,
  );
}

LinearGradient buildGradient(Color first, Color second) {
  return LinearGradient(
      begin: Alignment(-1.0, -2.0),
      end: Alignment(1.0, 2.0),
      colors: [first, second]);
}

Color buildColor(String type) {
  switch (type) {
    case "bug":
      return Color(0xff84C400);
    case "dark":
      return Color(0xff5B5366);
    case "dragon":
      return Color(0xff0070CA);
    case "electric":
      return Color(0xffE2BD00);
    case "fairy":
      return Color(0xffFB8AEC);
    case "fighting":
      return Color(0xffE12C6A);
    case "fire":
      return Color(0xffFF983F);
    case "flying":
      return Color(0xff8AACE4);
    case "ghost":
      return Color(0xff4B6AB3);
    case "grass":
      return Color(0xff35C04A);
    case "ground":
      return Color(0xffE97333);
    case "ice":
      return Color(0xff4BD2C1);
    case "normal":
      return Color(0xff929BA3);
    case "poison":
      return Color(0xffB667CF);
    case "psychic":
      return Color(0xffFF6676);
    case "rock":
      return Color(0xffC9B787);
    case "steel":
      return Color(0xff5A8FA3);
    case "water":
      return Color(0xff3393DD);
    default:
      return Colors.black;
  }
}
