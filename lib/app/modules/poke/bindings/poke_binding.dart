import 'package:get/get.dart';

import '../controllers/poke_controller.dart';

class PokeBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<PokeController>(
      () => PokeController(),
    );
  }
}
