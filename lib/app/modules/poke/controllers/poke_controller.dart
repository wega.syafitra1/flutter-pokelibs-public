import 'package:get/get.dart';
import '../models/poke_model.dart';
import '../models/pokemon_data.dart';
import 'package:http/http.dart' as http;

class PokeController extends GetxController {
  //TODO: Implement PokeController
  final count = 0.obs;
  var pokemonList = <Pokemon>[].obs;
  var pokeURLList = <PokeURL>[].obs;
  var currentPoke = <Pokemon>[].obs;
  void fetchPoke() async {
    // var client = http.Client();
    var output = <Pokemon>[];
    var response = await http.get(
      Uri.parse("https://pokeapi.co/api/v2/pokemon?limit=25&offset=0"),
    );

    if (response.statusCode == 200) {
      var body = response.body;
      var objets = pokeURLFromJson(body);
      pokeURLList.value = [objets];
      // print(objets.results[0].name);

      for (Results res in objets.results) {
        var response_poke = await http.get(Uri.parse("${res.url}"));

        if (response_poke.statusCode == 200) {
          var body2 = response_poke.body;
          var poke = pokemonFromJson(body2);
          output.add(poke);
          // print('success');
        }

        // print(res.name + ", " + res.url);
        // print(poke.name! + ", " + poke.stats![0].baseStat.toString());
        // print(response_poke.body);
      }
    } else {
      // print('error');
      // print("something happened");
    }
    pokemonList.value = output;
    // return output;
  }

  void fetchMorePokemon(String next) async {
    var client = http.Client();
    var output = <Pokemon>[];
    var response = await http.get(
      Uri.parse(next),
    );

    if (response.statusCode == 200) {
      var body = response.body;
      var objets = pokeURLFromJson(body);
      pokeURLList.value = [objets];
      // print(objets.results[0].name);

      for (Results res in objets.results) {
        var response_poke = await http.get(Uri.parse("${res.url}"));

        if (response_poke.statusCode == 200) {
          var body2 = response_poke.body;
          var poke = pokemonFromJson(body2);
          pokemonList.add(poke);
        }
      }
    } else {
      // print("something happened");
    }
    // pokemonList.value = output;
    // return output;
  }

  void fetchPokeURL() async {
    var client = http.Client();
    var output = <PokeURL>[];
    var response = await http.get(
      Uri.parse("https://pokeapi.co/api/v2/pokemon?limit=500&offset=1"),
    );

    if (response.statusCode == 200) {
      var body = response.body;
      var objets = pokeURLFromJson(body);
      pokeURLList.add(objets);
      // print(objets.results[0].name);

    } else {
      // print("something happened");
    }

    // return output;
  }

  Future<Pokemon?> fetchSpesPokemon(String url) async {
    var response = await http.get(
      Uri.parse("${url}"),
    );

    if (response.statusCode == 200) {
      var body = response.body;
      var objets = pokemonFromJson(body);
      return objets;
      // print(objets.results[0].name);

    } else {
      // print("something happened");
      return null;
    }
  }

  @override
  void onInit() {
    // fetchPokeURL();
    fetchPoke();
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}
  void increment() => count.value++;
}
