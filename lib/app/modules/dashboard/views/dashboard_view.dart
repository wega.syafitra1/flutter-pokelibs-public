import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/dashboard_controller.dart';
import 'package:transparent_image/transparent_image.dart';

class DashboardView extends GetView<DashboardController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'PokeLibs',
          ),
          elevation: 0,
          // backgroundColor: Color(0x00),
        ),
        body: ListView(
          padding: EdgeInsets.all(15),
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 20),
              child: Text(
                "All Pokemons",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                buildCont("All Pokemons", Color(0xffA858D8),
                    "https://www.freepnglogos.com/uploads/pokeball-png/pokeball-alexa-style-blog-pokemon-inspired-charmander-daily-8.png"),
              ],
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
              child: Text(
                "Pokemon Types",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                buildCont("Fire", Color(0xffFF983F),
                    "https://upload.wikimedia.org/wikipedia/commons/thumb/5/56/Pok%C3%A9mon_Fire_Type_Icon.svg/768px-Pok%C3%A9mon_Fire_Type_Icon.svg.png"),
                buildCont('Electric', Color(0xffFBD200),
                    "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Pok%C3%A9mon_Electric_Type_Icon.svg/768px-Pok%C3%A9mon_Electric_Type_Icon.svg.png"),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                buildCont("Dragon", Color(0xff0070CA),
                    "https://upload.wikimedia.org/wikipedia/commons/thumb/a/a6/Pok%C3%A9mon_Dragon_Type_Icon.svg/768px-Pok%C3%A9mon_Dragon_Type_Icon.svg.png"),
                buildCont('Water', Color(0xff3393DD),
                    "https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/Pok%C3%A9mon_Water_Type_Icon.svg/800px-Pok%C3%A9mon_Water_Type_Icon.svg.png"),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                buildCont("Rock", Color(0xffC9B787),
                    "https://upload.wikimedia.org/wikipedia/commons/thumb/b/bb/Pok%C3%A9mon_Rock_Type_Icon.svg/768px-Pok%C3%A9mon_Rock_Type_Icon.svg.png"),
                buildCont('Leaf', Color(0xff35C04A),
                    "https://upload.wikimedia.org/wikipedia/commons/thumb/f/f6/Pok%C3%A9mon_Grass_Type_Icon.svg/768px-Pok%C3%A9mon_Grass_Type_Icon.svg.png"),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                buildCont("Fighting", Color(0xffE12C6A),
                    "https://upload.wikimedia.org/wikipedia/commons/thumb/b/be/Pok%C3%A9mon_Fighting_Type_Icon.svg/768px-Pok%C3%A9mon_Fighting_Type_Icon.svg.png"),
                buildCont('Poison', Color(0xffB667CF),
                    "https://upload.wikimedia.org/wikipedia/commons/thumb/c/c4/Pok%C3%A9mon_Poison_Type_Icon.svg/768px-Pok%C3%A9mon_Poison_Type_Icon.svg.png"),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                buildCont("Dark", Color(0xff5B5366),
                    "https://upload.wikimedia.org/wikipedia/commons/thumb/0/09/Pok%C3%A9mon_Dark_Type_Icon.svg/768px-Pok%C3%A9mon_Dark_Type_Icon.svg.png"),
                buildCont('Steel', Color(0xff5A8FA3),
                    "https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Pok%C3%A9mon_Steel_Type_Icon.svg/768px-Pok%C3%A9mon_Steel_Type_Icon.svg.png"),
              ],
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
              child: Text(
                "Habitats",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                buildCont("Rare", Color(0xffA858D8),
                    "https://cdn2.bulbagarden.net/upload/6/69/Rare_Habitat.png"),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                buildCont("Grassland", Color(0xff6BB247),
                    "https://cdn2.bulbagarden.net/upload/a/a7/Grassland_Habitat.png"),
                buildCont('Forest', Color(0xff489078),
                    "https://cdn2.bulbagarden.net/upload/b/b2/Forest_Habitat.png"),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                buildCont("Water Edge", Color(0xff98D8E8),
                    "https://cdn2.bulbagarden.net/upload/4/49/Waters-edge_Habitat.png"),
                buildCont('Sea', Color(0xff5C80E5),
                    "https://cdn2.bulbagarden.net/upload/3/3b/Sea_Habitat.png"),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                buildCont("Cave", Color(0xff3E3B3E),
                    "https://cdn2.bulbagarden.net/upload/d/d5/Cave_Habitat.png"),
                buildCont('Mountain', Color(0xff907C4C),
                    "https://cdn2.bulbagarden.net/upload/f/f7/Mountain_Habitat.png"),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                buildCont("Urban", Color(0xff5B5366),
                    "https://cdn2.bulbagarden.net/upload/1/1a/Urban_Habitat.png"),
                buildCont('Rough Terrain', Color(0xffB77962),
                    "https://cdn2.bulbagarden.net/upload/9/9e/Rough-terrain_Habitat.png"),
              ],
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
              child: Text(
                "Generation",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
            ),
            expndButton(
              buildCont("Generation I (Red & Blue)", Color(0xff792C1B),
                  "https://static.wikia.nocookie.net/pokemon/images/e/e2/Pokemon_Red.jpg/revision/latest?cb=20200620222725"),
            ),
            expndButton(
              buildCont(
                  "Generation II (Gold, Silver, Crystal)",
                  Color(0xffE37729),
                  "https://static.wikia.nocookie.net/pokemon/images/1/17/Pokemon_gold.jpg/revision/latest?cb=20081012165158"),
            ),
            expndButton(
              buildCont("Generation III (Ruby & Sapphire)", Color(0xffBD5350),
                  "https://static.wikia.nocookie.net/pokemon/images/c/c4/Ruby_boxart.jpg/revision/latest?cb=20081029170700"),
            ),
            expndButton(
              buildCont("Generation IV (Diamond & Pearl)", Color(0xff3D64AC),
                  "https://static.wikia.nocookie.net/pokemon/images/2/22/DiamondUS.jpg/revision/latest?cb=20110219042350"),
            ),
            expndButton(
              buildCont("Generation V (Black & White)", Color(0xff090909),
                  "https://static.wikia.nocookie.net/pokemon/images/f/fb/Black_boxart.png/revision/latest?cb=20120524005829"),
            ),
            expndButton(
              buildCont("Generation VI (X & Y)", Color(0xffAACCB0),
                  "https://static.wikia.nocookie.net/pokemon/images/4/45/Pokemon_X_Version_Boxart.png/revision/latest?cb=20201120113848"),
            ),
            expndButton(
              buildCont("Generation VII (Sun & Moon)", Color(0xffB68E4C),
                  "https://static.wikia.nocookie.net/pokemon/images/0/09/Sun_English_Boxart.png/revision/latest?cb=20200626092513"),
            ),
            expndButton(
              buildCont("Generation VIII (Sword & Shield)", Color(0xff39578C),
                  "https://static.wikia.nocookie.net/pokemon/images/6/67/Pok%C3%A9mon_Sword_Boxart.png/revision/latest?cb=20190606175928"),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
              child: Text(
                "Extras",
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20),
              ),
            ),
            expndButton(
              buildCont("Cool Pokemons", Color(0xffBD9C0F),
                  "https://www.freepnglogos.com/uploads/pokeball-png/pokeball-alexa-style-blog-pokemon-inspired-charmander-daily-8.png"),
            ),
            expndButton(
              buildCont("Strongest Pokemons", Color(0xffC23426),
                  "http://squifamily.com/Pokemon/Shadow/mewtu.png"),
            ),
          ],
        ));
  }
}

Row expndButton(Expanded exp) {
  return Row(
    children: [exp],
  );
}

Expanded buildCont(String title, Color color, String icoLink) {
  return Expanded(
      child: ElevatedButton.icon(
          onPressed: () {},
          icon: FadeInImage.memoryNetwork(
            placeholder: kTransparentImage,
            height: 50,
            image: icoLink,
          ),
          label: Text(title),
          style: ElevatedButton.styleFrom(
              primary: color,
              elevation: 0,
              minimumSize: Size(double.infinity, 80))));
}
