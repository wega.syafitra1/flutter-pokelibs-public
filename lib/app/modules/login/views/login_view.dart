import 'package:fireflutter/app/controllers/auth_controller.dart';
import 'package:fireflutter/app/routes/app_pages.dart';
import 'package:fireflutter/main.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:get/get.dart';
import 'package:http/http.dart';

import '../controllers/login_controller.dart';

class LoginView extends GetView<LoginController> {
  final emailC = TextEditingController();
  final passwordC = TextEditingController();
  final authC = Get.find<AuthController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Login'),
          centerTitle: true,
          elevation: 0,
          backgroundColor: Color(0x00),
          foregroundColor: Colors.teal,
        ),
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              children: [
                Image(
                  image: AssetImage("assets/pokelibs.png"),
                  fit: BoxFit.cover,
                  height: 200,
                ),
                TextField(
                  controller: emailC,
                  decoration: InputDecoration(
                    labelText: "Email",
                    border: OutlineInputBorder(),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.black,
                      ),
                    ),
                    prefixIcon: Icon(Icons.person, size: 40),
                  ),
                  enableSuggestions: true,
                ),
                SizedBox(height: 10),
                TextField(
                  controller: passwordC,
                  decoration: InputDecoration(
                    labelText: "Password",
                    border: OutlineInputBorder(),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(
                        color: Colors.black,
                      ),
                    ),
                    prefixIcon: Icon(Icons.lock, size: 40),
                  ),
                  obscureText: true,
                  enableSuggestions: false,
                  autocorrect: false,
                ),
                SizedBox(
                  height: 70,
                ),
                ElevatedButton(
                  onPressed: () {
                    try {
                      authC.login(emailC.text, passwordC.text, context);
                    } catch (e) {}
                    // Get.to(Routes.HOME);
                  },
                  child: Text("Login"),
                  style: ElevatedButton.styleFrom(
                      minimumSize: Size(double.infinity, 50)),
                ),
                SizedBox(
                  height: 20,
                ),
                Row(children: <Widget>[
                  Expanded(child: div),
                  SizedBox(width: 30),
                  Text("OR"),
                  SizedBox(width: 30),
                  Expanded(child: div),
                ]),
                SizedBox(
                  height: 20,
                ),
                ElevatedButton.icon(
                  onPressed: () {
                    try {
                      authC.signInWithGoogle(context);
                    } catch (e) {
                      var snackBar = SnackBar(
                          content: Text("Select a google account"),
                          backgroundColor: Colors.red);
                      ScaffoldMessenger.of(context).showSnackBar(snackBar);
                    }
                  },
                  icon: Icon(FontAwesomeIcons.google),
                  label: Text("Login With Google"),
                  style: ElevatedButton.styleFrom(
                      minimumSize: Size(double.infinity, 50)),
                )
              ],
            ),
          ),
        ));
  }
}
