// import 'dart:html';

import 'package:fireflutter/app/routes/app_pages.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/getstarted_controller.dart';

class GetstartedView extends GetView<GetstartedController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(50.0),
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image(
                  image: AssetImage("assets/pokelibs.png"),
                  fit: BoxFit.cover,
                  height: 400,
                ),
                Text(
                  'Welcome To PokeLibs ',
                  style: TextStyle(fontSize: 14),
                ),
                Text(
                  "The Biggest Pokemon Encyclopedia",
                  style: TextStyle(fontSize: 14),
                ),
                SizedBox(height: 50),
                ElevatedButton(
                  onPressed: () {
                    Get.toNamed(Routes.LOGIN);
                  },
                  child: Text("Login"),
                  style: ElevatedButton.styleFrom(
                    minimumSize: Size(double.infinity, 40),
                  ),
                ),
                OutlinedButton(
                  onPressed: () {
                    Get.toNamed(Routes.SIGNUP);
                  },
                  child: Text(
                    "Signup",
                    style: TextStyle(color: Colors.teal),
                  ),
                  style: ElevatedButton.styleFrom(
                      elevation: 1,
                      minimumSize: Size(double.infinity, 40),
                      onPrimary: Colors.teal.shade100,
                      primary: Colors.white),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
