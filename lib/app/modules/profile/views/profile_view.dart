import 'package:firebase_auth/firebase_auth.dart';
import 'package:fireflutter/app/controllers/auth_controller.dart';
import 'package:fireflutter/app/modules/home/views/loading.dart';
import 'package:fireflutter/app/routes/app_pages.dart';
import 'package:flutter/material.dart';

import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';

import '../controllers/profile_controller.dart';

class ProfileView extends GetView<ProfileController> {
  final authC = Get.find<AuthController>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('My Profile'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(30.0),
        child: Center(
          child: Column(
            children: [
              ProfileDetail(),
              SizedBox(
                height: 30,
              ),
              OutlinedButton.icon(
                onPressed: () {
                  authC.logout();
                },
                label: Text("LogOut"),
                icon: Icon(Icons.logout),
                style: OutlinedButton.styleFrom(
                    side: BorderSide(width: .15),
                    primary: Colors.red,
                    minimumSize: Size(double.infinity, 50)),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class ProfileDetail extends StatelessWidget {
  // const AppAuth({Key? key}) : super(key: key);
  final authC = Get.find<AuthController>();
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<User?>(
        stream: authC.streamAuthStatus(),
        builder: (context, snapshot) {
          // print(snapshot);
          if (snapshot.connectionState == ConnectionState.active) {
            return Column(
              // height: 200,
              children: [
                CircleAvatar(
                  radius: 40,
                  backgroundImage: NetworkImage(snapshot.data == null
                      ? "https://www.pngitem.com/pimgs/m/576-5768680_avatar-png-icon-person-icon-png-free-transparent.png"
                      : snapshot.data!.photoURL == null
                          ? "https://www.pngitem.com/pimgs/m/576-5768680_avatar-png-icon-person-icon-png-free-transparent.png"
                          : snapshot.data!.photoURL.toString()),
                ),
                SizedBox(height: 20),
                Text(
                    "${snapshot.data != null ? snapshot.data!.displayName : "Kosong"}",
                    style: TextStyle(fontSize: 20)),
                SizedBox(
                  height: 30,
                ),
                Container(
                  height: 323,
                  width: 350,
                  child: Column(
                    children: [
                      Card(
                        child: ListTile(
                          leading: Icon(
                            Icons.catching_pokemon,
                            size: 50,
                            color: Colors.red.shade300,
                          ),
                          title: Text('Username'),
                          subtitle: Text(
                              '${snapshot.data != null ? snapshot.data!.displayName : "Kosong"}'),
                          // isThreeLine: true,
                        ),
                      ),
                      Card(
                        child: ListTile(
                          leading: Icon(
                            Icons.mail,
                            size: 50,
                            color: Colors.orange.shade300,
                          ),
                          title: Text('Email'),
                          subtitle: Text(
                              '${snapshot.data != null ? snapshot.data!.email : "Kosong"}'),
                          // isThreeLine: true,
                        ),
                      ),
                      Card(
                        child: ListTile(
                          leading: Icon(
                            Icons.schedule,
                            size: 50,
                            color: Colors.blue.shade300,
                          ),
                          title: Text('Waktu Terakhir Login'),
                          subtitle: Text(
                              '${snapshot.data == null ? "Kosong" : snapshot.data!.metadata.lastSignInTime.toString()}'),
                          // isThreeLine: true,
                        ),
                      ),
                      Card(
                        child: ListTile(
                          leading: Icon(
                            Icons.calendar_today,
                            size: 50,
                            color: Colors.green.shade300,
                          ),
                          title: Text('Waktu Daftar'),
                          subtitle: Text(
                              '${snapshot.data == null ? "Kosong" : snapshot.data!.metadata.creationTime}'),
                          // isThreeLine: true,
                        ),
                      ),
                    ],
                  ),
                  decoration: BoxDecoration(
                      border: Border.all(width: .15),
                      borderRadius: BorderRadius.all(Radius.circular(10))),
                )
              ],
            );
          } else {
            return CircularProgressIndicator();
          }
        });
  }
}
