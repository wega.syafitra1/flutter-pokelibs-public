import 'package:fireflutter/app/controllers/auth_controller.dart';
import 'package:fireflutter/app/modules/dashboard/views/dashboard_view.dart';
import 'package:fireflutter/app/modules/poke/views/poke_view.dart';
import 'package:fireflutter/app/modules/profile/views/profile_view.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:get/get.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  final authC = Get.find<AuthController>();
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(builder: (controller) {
      return Scaffold(
        // extendBodyBehindAppBar: true,
        body: SafeArea(
            child: IndexedStack(
          index: controller.tabIndex,
          children: [DashboardView(), PokeView(), ProfileView()],
        )),
        bottomNavigationBar: BottomNavigationBar(
          onTap: controller.changeTab,
          currentIndex: controller.tabIndex,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
            ),
            BottomNavigationBarItem(
                icon: Icon(Icons.catching_pokemon), label: 'PokeLib'),
            BottomNavigationBarItem(icon: Icon(Icons.person), label: "Profile")
          ],
        ),
      );
    });
  }
} 

/*class HomeView extends StatefulWidget {
  HomeView({Key? key}) : super(key: key);

  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  final authC = Get.find<AuthController>();
  int _currentIndex = 0;
  final _inactiveColor = Colors.grey;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (context) {
        return Scaffold(
            appBar: AppBar(
              title: Text('Home'),
              centerTitle: true,
            ),
            body: SafeArea(
                child: IndexedStack(
              children: [
                Padding(
                  padding: EdgeInsets.all(40),
                  child: Column(
                    children: [
                      Text(
                        'HomeView is working',
                        style: TextStyle(fontSize: 20),
                      ),
                      FloatingActionButton(
                          onPressed: () => authC.logout(),
                          child: Icon(Icons.logout))
                    ],
                  ),
                ),
                PokeView(),
                DashboardView(),
                ProfileView()
              ],
            ))

            // bottomNavigationBar: ,
            );
      },
    );
  }
}*/
