import 'package:flutter/material.dart';

class LoadingApp extends StatelessWidget {
  const LoadingApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text('waiting'),
        ),
        body: CircularProgressIndicator(),
      ),
    );
  }
}
