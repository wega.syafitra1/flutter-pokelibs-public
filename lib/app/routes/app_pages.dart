import 'package:get/get.dart';

import 'package:fireflutter/app/modules/dashboard/bindings/dashboard_binding.dart';
import 'package:fireflutter/app/modules/dashboard/views/dashboard_view.dart';
import 'package:fireflutter/app/modules/getstarted/bindings/getstarted_binding.dart';
import 'package:fireflutter/app/modules/getstarted/views/getstarted_view.dart';
import 'package:fireflutter/app/modules/home/bindings/home_binding.dart';
import 'package:fireflutter/app/modules/home/views/home_view.dart';
import 'package:fireflutter/app/modules/login/bindings/login_binding.dart';
import 'package:fireflutter/app/modules/login/views/login_view.dart';
import 'package:fireflutter/app/modules/poke/bindings/poke_binding.dart';
import 'package:fireflutter/app/modules/poke/views/poke_view.dart';
import 'package:fireflutter/app/modules/poke_detail/bindings/poke_detail_binding.dart';
import 'package:fireflutter/app/modules/poke_detail/views/poke_detail_view.dart';
import 'package:fireflutter/app/modules/profile/bindings/profile_binding.dart';
import 'package:fireflutter/app/modules/profile/views/profile_view.dart';
import 'package:fireflutter/app/modules/signup/bindings/signup_binding.dart';
import 'package:fireflutter/app/modules/signup/views/signup_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.HOME;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.LOGIN,
      page: () => LoginView(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: _Paths.SIGNUP,
      page: () => SignupView(),
      binding: SignupBinding(),
    ),
    GetPage(
      name: _Paths.PROFILE,
      page: () => ProfileView(),
      binding: ProfileBinding(),
    ),
    GetPage(
      name: _Paths.GETSTARTED,
      page: () => GetstartedView(),
      binding: GetstartedBinding(),
    ),
    GetPage(
      name: _Paths.POKE,
      page: () => PokeView(),
      binding: PokeBinding(),
    ),
    GetPage(
      name: _Paths.DASHBOARD,
      page: () => DashboardView(),
      binding: DashboardBinding(),
    ),
    GetPage(
      name: _Paths.POKE_DETAIL,
      page: () => PokeDetailView(),
      binding: PokeDetailBinding(),
    ),
  ];
}
