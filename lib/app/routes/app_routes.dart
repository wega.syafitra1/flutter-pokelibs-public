part of 'app_pages.dart';
// DO NOT EDIT. This is code generated via package:get_cli/get_cli.dart

abstract class Routes {
  Routes._();

  static const HOME = _Paths.HOME;
  static const LOGIN = _Paths.LOGIN;
  static const SIGNUP = _Paths.SIGNUP;
  static const PROFILE = _Paths.PROFILE;
  static const GETSTARTED = _Paths.GETSTARTED;
  static const POKE = _Paths.POKE;
  static const DASHBOARD = _Paths.DASHBOARD;
  static const POKE_DETAIL = _Paths.POKE_DETAIL;
}

abstract class _Paths {
  static const HOME = '/home';
  static const LOGIN = '/login';
  static const SIGNUP = '/signup';
  static const PROFILE = '/profile';
  static const GETSTARTED = '/getstarted';
  static const POKE = '/poke';
  static const DASHBOARD = '/dashboard';
  static const POKE_DETAIL = '/poke-detail';
}
